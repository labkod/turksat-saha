<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{

    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    public function town()
    {
        return $this->belongsTo('App\Town');
    }

    public function getAttachedStreetAttribute()
    {
        if($this->street)
            return $this->street;
        elseif($this->district)
            return $this->district->street;
        else
            return null;
    }

    public function getAttachedTownAttribute()
    {
        if($this->town)
            return $this->town;
        elseif($this->attachedStreet)
            return $this->attachedStreet->town;
        elseif($this->district)
            return $this->district->street->town;
        else
            return null;
    }

    public function checks()
    {
        return $this->hasMany('App\ApartmentCheck');
    }

    public function user_checks()
    {
        return $this->checks()->where('user_id', auth()->user()->id);
    }

    public function homes()
    {
        return $this->hasMany('App\Home');
    }

}
