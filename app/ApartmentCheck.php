<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApartmentCheck extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function apartment()
    {
        return $this->belongsTo('App\Apartment');
    }

    public function options()
    {
        return $this->belongsToMany('App\CheckOption');
    }

    public function pictures()
    {
        return $this->hasMany('App\ApartmentCheckPicture');
    }

    public function getIsLockedAttribute()
    {
        if(auth()->user()->hasRole('yonetici'))
            return false;
        $created = $this->created_at;
        $now     = Carbon::now();

        return $now->diffInMinutes($created) > setting('action_lock');
    }

}
