<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApartmentCheckPicture extends Model
{

    public function check()
    {
        return $this->belongsTo('App\ApartmentCheck');
    }

}
