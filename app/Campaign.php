<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{

    public function packages()
    {
        return $this->belongsToMany('App\Package');
    }

    public function sells()
    {
        return $this->hasMany('App\Sell');
    }

    public function totalServicePackage(Service $service)
    {
        $total = 0;
        foreach($this->packages as $package)
            if($package->service->id == $service->id)
                $total++;
        return $total;
    }

}
