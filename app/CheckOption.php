<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckOption extends Model
{

    public function checks()
    {
        return $this->belongsToMany('App\ApartmentCheck');
    }

}
