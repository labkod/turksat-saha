<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    public function apartments()
    {
        return $this->hasMany('App\Apartment');
    }
}
