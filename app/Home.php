<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{

    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function pictures()
    {
        return $this->hasMany(HomePicture::class);
    }

    public function sells()
    {
        return $this->hasMany(Sell::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getHumanCommitmentAttribute()
    {
        if($this->commitment){
            $exp = explode("-", $this->commitment);
            return $exp[2].".".$exp[1].".".$exp[0];
        }
        return null;
    }

    public function getIsLockedAttribute()
    {
        if(auth()->user()->hasRole('yonetici'))
            return false;
        $created = $this->created_at;
        $now     = Carbon::now();

        return $now->diffInMinutes($created) > setting('action_lock');
    }

}
