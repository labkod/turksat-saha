<?php

namespace App\Http\Controllers\Action;

use App\Apartment;
use App\ApartmentCheck;
use App\ApartmentCheckPicture;
use App\Home;
use App\HomePicture;
use App\Http\Requests\Action\ActionCreateRequest;
use App\Sell;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActionsController extends Controller
{
    public function create()
    {
        return view('action.action.create');
    }

    protected function newApartment($name, $district, $street, $town, $visorName, $visorSurname, $visorPhone) {
        $apartment = new Apartment();
        if($district != "not")
            $apartment->district_id = $district;
        if($street != "not")
            $apartment->street_id = $street;
        if($town != "not")
            $apartment->town_id = $town;
        $apartment->name = $name;
        $apartment->visor_name = $visorName;
        $apartment->visor_surname = $visorSurname;
        $apartment->visor_phone = $visorPhone;
        $apartment->save();

        return $apartment;
    }

    protected function newApartmentCheck($apartment, $notes, $options, $pictures) {
        $check = new ApartmentCheck();
        $check->apartment_id = $apartment;
        $check->user_id = auth()->user()->id;
        $check->notes = $notes;
        $check->save();
        $check->options()->sync($options);

        if($pictures) {
            foreach ($pictures as $pic) {
                $uploadName = str_random(16) . "." . $pic->getClientOriginalExtension();
                $pic->move(public_path('upload/checks/apartment'), $uploadName);
                $uploadPath = "upload/checks/apartment/" . $uploadName;

                $picture = new ApartmentCheckPicture();
                $picture->apartment_check_id = $check->id;
                $picture->name = $pic->getClientOriginalName();
                $picture->file = $uploadPath;
                $picture->save();
            }
        }

        return $check;
    }

    public function store(ActionCreateRequest $request)
    {
        $completed = [
            "apartment" => null,
            "apartmentCheck" => null,
            "homes" => [],
            "sells" => [],
        ];

        $apartmentId = $request->get('apartment_id');
        if(!is_numeric($apartmentId)){
            $apartment = $this->newApartment(
                $request->get('apartment_id'),
                $request->get('district_id'),
                $request->get('street_id'),
                $request->get('town_id'),
                $request->get('visor_name'),
                $request->get('visor_surname'),
                $request->get('visor_phone'));
            $completed['apartment'] = $apartment;
            $apartmentId = $apartment->id;
        }

        $apartmentCheck = $this->newApartmentCheck(
            $apartmentId,
            $request->get('apartment_check_notes'),
            $request->get('apartment_check_option'),
            $request->file('apartment_check_pictures')
        );
        $completed['apartmentCheck'] = $apartmentCheck;

        if($request->has('home') && $request->get('home') && count($request->get('home')) > 0){
        foreach($request->get('home') as $home){
            $commitment = null;
            if($home['commitment']){
                $exp = explode(".", $home['commitment']);
                if(count($exp) == 3){
                    $commitment = $exp[2]."-".$exp[1]."-".$exp[0];
                }
            }

            $homeCheck = new Home();
            $homeCheck->name = $home['name'];
            $homeCheck->surname = $home['surname'];
            $homeCheck->identity = isset($home['identity']) ? $home['identity'] : null;
            $homeCheck->phone = $home['phone'];
            $homeCheck->email = $home['email'];
            $homeCheck->commitment = $commitment;
            $homeCheck->notes = $home['notes'];
            $homeCheck->apartment_id = $apartmentId;
            $homeCheck->provider_id = $home['provider_id'] != "0" ? $home['provider_id'] : null;
            $homeCheck->user_id = auth()->user()->id;
            $homeCheck->home_no = $home['home_no'];
            $homeCheck->save();

            if(isset($home['pictures'])) {
                foreach ($home['pictures'] as $pic) {

                    $uploadName = str_random(16) . "." . $pic->getClientOriginalExtension();
                    $pic->move(public_path('upload/checks/home'), $uploadName);
                    $uploadPath = "upload/checks/home/" . $uploadName;

                    $picture = new HomePicture();
                    $picture->home_id = $homeCheck->id;
                    $picture->name = $pic->getClientOriginalName();
                    $picture->file = $uploadPath;
                    $picture->save();
                }
            }

            if($home['sell'] == 1){

                $sell = new Sell();
                $sell->home_id = $homeCheck->id;
                $sell->campaign_id = $home['campaign_id'] != 0 ? $home['campaign_id'] : null;
                $sell->user_id = auth()->user()->id;
                $sell->save();

                $packageInput = $home['campaign_id'] != 0 ? "campaignPackages" : "packages";
                $sell->packages()->attach($home[$packageInput]);

                $completed['sells'][] = $sell;
            }

            $completed['homes'][] = $homeCheck;
        }
        }
        return view('action.action.success', [
            "apartment" => $completed['apartment'],
            "apartmentCheck" => $completed['apartmentCheck'],
            "homes" => $completed['homes'],
            "sells" => $completed['sells'],
        ]);
    }
}
