<?php

namespace App\Http\Controllers\Action;

use App\Apartment;
use App\ApartmentCheck;
use App\ApartmentCheckPicture;
use App\Http\Requests\Action\ApartmentCheckCreateRequest;
use App\Http\Requests\Action\ApartmentCheckDestroyMultipleRequest;
use App\Http\Requests\Action\ApartmentCheckEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApartmentCheckController extends Controller
{
    public function index(Apartment $apartment) {

        $checks = auth()->user()->apartment_checks()->where('apartment_id', $apartment->id)->orderBy('id', 'ASC')->get();

        return view('action.apartment.check.index', [
            "apartment" => $apartment,
            "checks"    => $checks,
        ]);
    }

    public function create(Apartment $apartment) {

        return view('action.apartment.check.create', [
            "apartment" => $apartment,
        ]);

    }

    public function store(ApartmentCheckCreateRequest $request, Apartment $apartment) {

        $check = new ApartmentCheck();
        $check->apartment_id = $apartment->id;
        $check->user_id = auth()->user()->id;
        $check->notes = $request->get('notes');
        $check->save();
        $check->options()->sync($request->get('option'));

        if($request->hasFile('pictures')) {
            foreach ($request->file('pictures') as $pic) {

                $uploadName = str_random(16) . "." . $pic->getClientOriginalExtension();
                $pic->move(public_path('upload/checks/apartment'), $uploadName);
                $uploadPath = "upload/checks/apartment/" . $uploadName;

                $picture = new ApartmentCheckPicture();
                $picture->apartment_check_id = $check->id;
                $picture->name = $pic->getClientOriginalName();
                $picture->file = $uploadPath;
                $picture->save();
            }
        }

        return redirect()->route('actions.apartment.check.index', $apartment->id);

    }

    public function show(Apartment $apartment, ApartmentCheck $check) {
        return view('action.apartment.check.show', [
            "apartment" => $apartment,
            "check"     => $check,
        ]);
    }

    public function edit(Apartment $apartment, ApartmentCheck $check) {
        if($check->isLocked) return redirect()->route('actions.apartment.check.index', $apartment->id);
        return view('action.apartment.check.edit', [
            "apartment" => $apartment,
            "check"     => $check,
        ]);
    }

    public function update(ApartmentCheckEditRequest $request, Apartment $apartment, ApartmentCheck $check) {
        if($check->isLocked) return redirect()->route('actions.apartment.check.index', $apartment->id);
        $check->notes = $request->get('notes');
        $check->save();
        $check->options()->sync($request->get('option'));

        if($request->get('removes')){
            foreach($request->get('removes') as $removeId) {
                ApartmentCheckPicture::where('id', $removeId)->delete();
            }
        }

        if($request->hasFile('pictures')){
            foreach($request->file('pictures') as $pic) {

                $uploadName = str_random(16).".".$pic->getClientOriginalExtension();
                $pic->move(public_path('upload/checks/apartment'),$uploadName);
                $uploadPath = "upload/checks/apartment/".$uploadName;

                $picture = new ApartmentCheckPicture();
                $picture->apartment_check_id = $check->id;
                $picture->name = $pic->getClientOriginalName();
                $picture->file = $uploadPath;
                $picture->save();
            }
        }

        return redirect()->route('actions.apartment.check.edit', [$apartment->id, $check->id]);
    }

    public function destroy(Apartment $apartment, ApartmentCheck $check)
    {
        if($check->isLocked) return redirect()->route('actions.apartment.check.index', $apartment->id);
        $check->delete();

        return redirect()->route('actions.apartment.check.index', $apartment->id);
    }

    public function destroyMultiple(ApartmentCheckDestroyMultipleRequest $request, Apartment $apartment)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $check = ApartmentCheck::findOrFail($selected);
            if($check->isLocked) return redirect()->route('actions.apartment.check.index', $apartment->id);
            $check->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
