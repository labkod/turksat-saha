<?php

namespace App\Http\Controllers\Action;

use App\Apartment;
use App\Http\Requests\Action\ApartmentCreateRequest;
use App\Http\Requests\Action\ApartmentDestroyMultipleRequest;
use App\Http\Requests\Action\ApartmentEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApartmentController extends Controller
{
    public function index()
    {
        $apartments = Apartment::latest()->paginate(20);
        return view('action.apartment.index', [
            'apartments' => $apartments
        ]);
    }

    public function create()
    {
        return view('action.apartment.create');
    }

    public function store(ApartmentCreateRequest $request) {
        $apartment = new Apartment();
        if($request->get('district_id') != "not")
            $apartment->district_id = $request->get('district_id');
        if($request->get('street_id') != "not")
            $apartment->street_id = $request->get('street_id');
        if($request->get('town_id') != "not")
            $apartment->town_id = $request->get('town_id');
        $apartment->name = $request->get('name');
        $apartment->visor_name = $request->get('visor_name');
        $apartment->visor_surname = $request->get('visor_surname');
        $apartment->visor_phone = $request->get('visor_phone');
        $apartment->save();

        return redirect()->route('actions.apartment.index');
    }

    public function edit(Apartment $apartment) {
        return view('action.apartment.edit', [
            "apartment" => $apartment,
        ]);
    }

    public function update(ApartmentEditRequest $request, Apartment $apartment) {
        if($request->get('district_id') != "not")
            $apartment->district_id = $request->get('district_id');
        if($request->get('street_id') != "not")
            $apartment->street_id = $request->get('street_id');
        if($request->get('town_id') != "not")
            $apartment->town_id = $request->get('town_id');
        $apartment->name = $request->get('name');
        $apartment->visor_name = $request->get('visor_name');
        $apartment->visor_surname = $request->get('visor_surname');
        $apartment->visor_phone = $request->get('visor_phone');
        $apartment->save();

        return redirect()->route('actions.apartment.index');
    }

    public function destroy(Apartment $apartment)
    {
        $apartment->delete();

        return redirect()->back();
    }

    public function destroyMultiple(ApartmentDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $apartment = Apartment::findOrFail($selected);
            $apartment->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
