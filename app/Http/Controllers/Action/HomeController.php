<?php

namespace App\Http\Controllers\Action;

use App\Home;
use App\HomePicture;
use App\Http\Requests\Action\HomeCreateRequest;
use App\Http\Requests\Action\HomeDestroyMultipleRequest;
use App\Http\Requests\Action\HomeEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {
        $homes = auth()->user()->homes()->latest()->paginate(20);

        return view('action.home.index', [
            "homes" => $homes,
        ]);
    }

    public function create() {
        return view('action.home.create', [

        ]);
    }

    public function store(HomeCreateRequest $request) {

        $commitment = null;
        if($request->has('commitment')){
            $exp = explode(".", $request->get('commitment'));
            if(count($exp) == 3){
                $commitment = $exp[2]."-".$exp[1]."-".$exp[0];
            }
        }

        $home = new Home;
        $home->name = $request->get('name');
        $home->surname = $request->get('surname');
        $home->identity = $request->get('identity');
        $home->phone = $request->get('phone');
        $home->email = $request->get('email');
        $home->commitment = $commitment;
        $home->notes = $request->get('notes');
        $home->apartment_id = $request->get('apartment_id');
        $home->provider_id = $request->get('provider_id') != "0" ? $request->get('provider_id') : null;
        $home->user_id = auth()->user()->id;
        $home->home_no = $request->get('home_no');
        $home->save();

        if($request->hasFile('pictures')) {
            foreach ($request->file('pictures') as $pic) {

                $uploadName = str_random(16) . "." . $pic->getClientOriginalExtension();
                $pic->move(public_path('upload/checks/home'), $uploadName);
                $uploadPath = "upload/checks/home/" . $uploadName;

                $picture = new HomePicture();
                $picture->home_id = $home->id;
                $picture->name = $pic->getClientOriginalName();
                $picture->file = $uploadPath;
                $picture->save();
            }
        }

        return redirect()->route('actions.home.index');

    }

    public function show(Home $home) {
        return view('action.home.show', [
            "home" => $home,
        ]);
    }

    public function edit(Home $home) {
        if($home->isLocked) return redirect()->route('actions.home.index');
        return view('action.home.edit', [
            "home" => $home,
        ]);
    }

    public function update(HomeEditRequest $request, Home $home) {
        if($home->isLocked) return redirect()->route('actions.home.index');
        $commitment = null;
        if($request->has('commitment')){
            $exp = explode(".", $request->get('commitment'));
            if(count($exp) == 3){
                $commitment = $exp[2]."-".$exp[1]."-".$exp[0];
            }
        }

        $home->name = $request->get('name');
        $home->surname = $request->get('surname');
        $home->identity = $request->get('identity');
        $home->phone = $request->get('phone');
        $home->email = $request->get('email');
        $home->commitment = $commitment;
        $home->notes = $request->get('notes');
        $home->apartment_id = $request->get('apartment_id');
        $home->provider_id = $request->get('provider_id') != 0 ? $request->get('provider_id') : null;
        $home->home_no = $request->get('home_no');
        $home->save();

        if($request->get('removes')){
            foreach($request->get('removes') as $removeId) {
                HomePicture::where('id', $removeId)->delete();
            }
        }

        if($request->hasFile('pictures')) {
            foreach ($request->file('pictures') as $pic) {

                $uploadName = str_random(16) . "." . $pic->getClientOriginalExtension();
                $pic->move(public_path('upload/checks/home'), $uploadName);
                $uploadPath = "upload/checks/home/" . $uploadName;

                $picture = new HomePicture();
                $picture->home_id = $home->id;
                $picture->name = $pic->getClientOriginalName();
                $picture->file = $uploadPath;
                $picture->save();
            }
        }

        return redirect()->route('actions.home.edit', $home->id);
    }

    public function destroy(Home $home)
    {
        if($home->isLocked) return redirect()->route('actions.home.index');
        $home->delete();

        return redirect()->route('actions.home.index');
    }

    public function destroyMultiple(HomeDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $home = Home::findOrFail($selected);
            if($home->isLocked) return redirect()->route('actions.home.index');
            $home->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
