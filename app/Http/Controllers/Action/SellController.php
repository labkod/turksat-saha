<?php

namespace App\Http\Controllers\Action;

use App\Apartment;
use App\Home;
use App\Http\Requests\Action\SellCreateRequest;
use App\Http\Requests\Action\SellEditRequest;
use App\Sell;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellController extends Controller
{
    public function index()
    {
        $sells = auth()->user()->sells()->latest()->paginate(20);

        return view('action.sell.index', [
            'sells' => $sells,
        ]);
    }

    public function create()
    {
        $homes = [0 => "Seçiniz"];

        foreach(Apartment::orderBy('name', 'ASC')->get() as $apartment){
            foreach($apartment->homes as $home){
                $homes[$home->id] = $apartment->name." - ".$home->home_no." numara - ".$home->name." ".$home->surname." (".$home->created_at->format('d.m.Y').")";
            }
        }

        return view('action.sell.create', [
            "homes" => $homes,
        ]);
    }

    public function store(SellCreateRequest $request) {

        $sell = new Sell();
        $sell->home_id = $request->get('home_id');
        $sell->campaign_id = $request->get('campaign_id') != 0 ? $request->get('campaign_id') : null;
        $sell->user_id = auth()->user()->id;
        $sell->save();

        $packageInput = $request->get('campaign_id') != 0 ? "campaignPackages" : "packages";
        $sell->packages()->attach($request->get($packageInput));

        return redirect()->route('actions.sell.index');

    }

    public function show(Sell $sell) {
        return view('action.sell.show', [
            "sell" => $sell,
        ]);
    }

    public function edit(Sell $sell) {
        if($sell->isLocked) return redirect()->route('actions.sell.index');
        $homes = [0 => "Seçiniz"];

        foreach(Apartment::orderBy('name', 'ASC')->get() as $apartment){
            foreach($apartment->homes as $home){
                $homes[$home->id] = $apartment->name." - ".$home->home_no." numara - ".$home->name." ".$home->surname." (".$home->created_at->format('d.m.Y').")";
            }
        }

        return view('action.sell.edit', [
            "sell" => $sell,
            "homes" => $homes,
        ]);
    }

    public function update(SellEditRequest $request, Sell $sell) {
        if($sell->isLocked) return redirect()->route('actions.sell.index');
        $sell->home_id = $request->get('home_id');
        $sell->campaign_id = $request->get('campaign_id') != 0 ? $request->get('campaign_id') : null;
        $sell->user_id = auth()->user()->id;
        $sell->save();

        $packageInput = $request->get('campaign_id') != 0 ? "campaignPackages" : "packages";
        $sell->packages()->sync($request->get($packageInput));

        return redirect()->route('actions.sell.edit', $sell->id);
    }

    public function destroy(Sell $sell)
    {
        if($sell->isLocked) return redirect()->route('actions.sell.index');
        $sell->delete();

        return redirect()->back();
    }

    public function destroyMultiple(Request $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $sell = Sell::findOrFail($selected);
            if($sell->isLocked) return redirect()->route('actions.sell.index');
            $sell->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
