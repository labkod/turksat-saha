<?php

namespace App\Http\Controllers\Action;

use App\CheckOption;
use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilController extends Controller
{
    public function apartments(Request $request) {
        $result = [];

        $search = $request->get('search');
        $district = District::find($request->get('district_id'));
        $result["results"] = [];

        if($search)
            $apartments = $district->apartments()->where('name', 'LIKE', '%'.$search.'%')->orderBy('name', 'ASC')->get();
        else
            $apartments = $district->apartments()->orderBy('name', 'ASC')->get();

        foreach($apartments as $apartment){
            $result["results"][] = [
                "text" => $apartment->name,
                "id"   => $apartment->id
            ];
        }

        return $result;
    }

    public function options() {
        return CheckOption::select('id', 'name')->get();
    }
}
