<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthSettingsUpdateRequest;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function login() {
        return view('auth.login');
    }

    public function doLogin(AuthLoginRequest $request) {
        if(auth()->attempt([
            "username" => $request->get('username'),
            "password" => $request->get('password'),
        ], $request->has('remember'))){
            return redirect()->route('dashboard.index');
        } else {
            return redirect()->back()->withInput()->withErrors(["username" => "Kullanıcı adı veya parola hatalı."]);
        }
    }

    public function logout() {
        auth()->logout();

        return redirect()->route('auth.login');
    }

    public function settings() {
        return view('auth.settings');
    }

    public function updateSettings(AuthSettingsUpdateRequest $request) {

        $user = auth()->user();
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        if($request->has('password'))
            $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('auth.settings');
    }

}
