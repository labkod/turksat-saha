<?php

namespace App\Http\Controllers\Index;

use App\Apartment;
use App\ApartmentCheck;
use App\ApartmentCheckPicture;
use App\Http\Requests\Action\ApartmentCheckCreateRequest;
use App\Http\Requests\Action\ApartmentCheckDestroyMultipleRequest;
use App\Http\Requests\Action\ApartmentCheckEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApartmentCheckController extends Controller
{
    public function index(Apartment $apartment) {
        $checks = $apartment->checks()->orderBy('id', 'ASC')->get();

        return view('index.apartment.check.index', [
            "apartment" => $apartment,
            "checks"    => $checks,
        ]);
    }

    public function show(Apartment $apartment, ApartmentCheck $check) {
        return view('index.apartment.check.show', [
            "apartment" => $apartment,
            "check"     => $check,
        ]);
    }
}
