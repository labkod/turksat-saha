<?php

namespace App\Http\Controllers\Index;

use App\Apartment;
use App\District;
use App\Street;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApartmentController extends Controller
{
    public function index(Request $request)
    {
        $townId     = $request->get('town_id');
        $streetId   = $request->get('street_id');
        $districtId = $request->get('district_id');

        $apartments = Apartment::latest();

        $town = Town::find($townId);
        if($town){
            $street = Street::find($streetId);
            if($street){
                $district = District::find($districtId);
                if($district){
                    $apartments = $apartments->where('district_id', $district->id);
                } else {
                    $apartments = $apartments->where('street_id', $street->id);
                }
            } else {
                $apartments = $apartments->where('town_id', $town->id);
            }
        }

        $apartments = $apartments->paginate(20);
        return view('index.apartment.index', [
            "apartments" => $apartments,

            "townId" => $townId,
            "streetId" => $streetId,
            "districtId" => $districtId,
        ]);
    }
}
