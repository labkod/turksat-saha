<?php

namespace App\Http\Controllers\Index;

use App\Apartment;
use App\District;
use App\Home;
use App\Street;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $apartments  = [0 => "Önce İlçe Seçiniz"];
        $townId      = $request->get('town_id');
        $streetId    = $request->get('street_id');
        $districtId  = $request->get('district_id');
        $apartmentId = $request->get('apartment_id');

        $homes = Home::latest();

        $town = Town::find($townId);
        if($town){
            $street = Street::find($streetId);
            if($street){
                $district = District::find($districtId);
                if($district){
                    $apartment = Apartment::find($apartmentId);
                    if($apartment){
                        $homes = $homes->where('apartment_id', $apartment->id);
                    } else {
                        $apartmentList = [];
                        foreach($district->apartments as $apart)
                            $apartmentList[] = $apart->id;
                        $homes = $homes->whereIn('apartment_id', $apartmentList);
                    }
                    $apartments = $district->apartments()->pluck('name', 'id')->toArray();
                } else {
                    $apartmentList = [];
                    foreach($street->apartments as $apart)
                        $apartmentList[] = $apart->id;
                    $homes = $homes->whereIn('apartment_id', $apartmentList);
                    if($street->districts->count() == 0){
                        $apartments = $street->apartments()->pluck('name', 'id')->toArray();
                    }
                }
            } else {
                $apartmentList = [];
                foreach($town->apartments as $apart)
                    $apartmentList[] = $apart->id;
                $homes = $homes->whereIn('apartment_id', $apartmentList);
                if($town->streets->count() == 0){
                    $apartments = $town->apartments()->pluck('name', 'id')->toArray();
                }
            }
        }

        $homes = $homes->paginate(20);
        return view('index.home.index', [
            "homes" => $homes,

            "apartments" => $apartments,
            "apartmentId" => $apartmentId,
            "townId" => $townId,
            "streetId" => $streetId,
            "districtId" => $districtId,
        ]);
    }

    public function show(Home $home) {
        return view('index.home.show', [
            "home" => $home,
        ]);
    }
}
