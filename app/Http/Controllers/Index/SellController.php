<?php

namespace App\Http\Controllers\Index;

use App\Apartment;
use App\District;
use App\Home;
use App\Sell;
use App\Street;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellController extends Controller
{
    public function index(Request $request)
    {
        $apartments  = [0 => "Önce İlçe Seçiniz"];
        $townId      = $request->get('town_id');
        $streetId    = $request->get('street_id');
        $districtId  = $request->get('district_id');
        $apartmentId = $request->get('apartment_id');
        $homeId      = $request->get('home_id');

        $sells = Sell::latest();

        $town = Town::find($townId);
        if($town){
            $street = Street::find($streetId);
            if($street){
                $district = District::find($districtId);
                if($district){
                    $apartment = Apartment::find($apartmentId);
                    if($apartment){
                        $home = Home::find($homeId);
                        if($home) {
                            $sells = $sells->where('home_id', $home->id);
                        } else {
                            $homeList = [];
                            foreach($apartment->homes as $home)
                                $homeList[] = $home->id;
                            $sells = $sells->whereIn('home_id', $homeList);
                        }
                    } else {
                        $homeList = [];
                        foreach($district->apartments as $apart)
                            foreach($apart->homes as $home)
                                $homeList[] = $home->id;
                        $sells = $sells->whereIn('home_id', $homeList);
                    }
                    $apartments = $district->apartments()->pluck('name', 'id')->toArray();
                } else {
                    $homeList = [];
                    foreach($street->apartments as $apart)
                        foreach($apart->homes as $home)
                            $homeList[] = $home->id;
                    $sells = $sells->whereIn('home_id', $homeList);
                    if($street->districts->count() == 0){
                        $apartments = $street->apartments()->pluck('name', 'id')->toArray();
                    }
                }
            } else {
                $homeList = [];
                foreach($town->apartments as $apart)
                    foreach($apart->homes as $home)
                        $homeList[] = $home->id;
                $sells = $sells->whereIn('home_id', $homeList);
                if($town->streets->count() == 0){
                    $apartments = $town->apartments()->pluck('name', 'id')->toArray();
                }
            }
        }

        $sells = $sells->paginate(20);

        return view('index.sell.index', [
            'sells' => $sells,

            "apartments" => $apartments,
            "apartmentId" => $apartmentId,
            "townId" => $townId,
            "streetId" => $streetId,
            "districtId" => $districtId,
            "homeId" => $homeId,
        ]);
    }

    public function show(Sell $sell) {
        return view('index.sell.show', [
            "sell" => $sell,
        ]);
    }
}
