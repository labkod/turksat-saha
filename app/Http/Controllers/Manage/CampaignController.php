<?php

namespace App\Http\Controllers\Manage;

use App\Campaign;
use App\Http\Requests\Manage\CampaignCreateRequest;
use App\Http\Requests\Manage\CampaignDestroyMultipleRequest;
use App\Http\Requests\Manage\CampaignEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    public function index()
    {
        $campaigns = Campaign::latest()->paginate(20);

        return view('manage.campaign.index', [
            "campaigns" => $campaigns
        ]);
    }

    public function create()
    {
        return view('manage.campaign.create');
    }

    public function store(CampaignCreateRequest $request)
    {
        $campaign = new Campaign;
        $campaign->name = $request->get('name');
        $campaign->save();

        $campaign->packages()->attach($request->get('packages'));

        return redirect()->route('manage.campaign.index');
    }

    public function edit(Campaign $campaign)
    {
        return view('manage.campaign.edit', [
            "campaign" => $campaign,
        ]);
    }

    public function update(CampaignEditRequest $request, Campaign $campaign)
    {
        $campaign->name = $request->get('name');
        $campaign->save();

        $campaign->packages()->sync($request->get('packages'));

        return redirect()->route('manage.campaign.index');
    }

    public function destroy(Campaign $campaign) {
        $campaign->delete();

        return redirect()->back();
    }

    public function destroyMultiple(CampaignDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $campaign = Campaign::findOrFail($selected);
            $campaign->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
