<?php

namespace App\Http\Controllers\Manage;

use App\District;
use App\Http\Requests\Manage\DistrictCreateRequest;
use App\Http\Requests\Manage\DistrictDestroyMultipleRequest;
use App\Http\Requests\Manage\DistrictEditRequest;
use App\Street;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function index(Town $town, Street $street)
    {
        $districts = $street->districts()->orderBy("name", "ASC")->paginate(50);
        return view('manage.district.index', [
            "town"      => $town,
            "street"    => $street,
            "districts" => $districts,
        ]);
    }

    public function store(DistrictCreateRequest $request, Town $town, Street $street)
    {
        $district            = new District;
        $district->name      = $request->get('name');
        $district->street_id = $street->id;
        $district->save();

        return redirect()->route('manage.location.street.district.index', [$town->id, $street->id]);
    }

    public function edit(Town $town, Street $street, District $district)
    {
        return view('manage.district.edit', [
            "town"     => $town,
            "street"   => $street,
            "district" => $district,
        ]);
    }

    public function update(DistrictEditRequest $request, Town $town, Street $street, District $district)
    {
        $district->name = $request->get('name');
        $district->save();

        return redirect()->route('manage.location.street.district.index', [$town->id, $street->id]);
    }

    public function destroy(Town $town, Street $street, District $district)
    {
        $district->delete();

        return redirect()->back();
    }

    public function destroyMultiple(DistrictDestroyMultipleRequest $request, Town $town, Street $street)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $d = District::findOrFail($selected);
            $d->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
