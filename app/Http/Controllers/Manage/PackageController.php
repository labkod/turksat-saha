<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\PackageCreateRequest;
use App\Http\Requests\Manage\PackageDestroyMultipleRequest;
use App\Http\Requests\Manage\PackageEditRequest;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::latest()->paginate(30);
        return view('manage.package.index', [
            "packages" => $packages,
        ]);
    }

    public function create()
    {
        return view('manage.package.create');
    }

    public function store(PackageCreateRequest $request) {
        $package = new Package;
        $package->name = $request->get('name');
        $package->service_id = $request->get('service_id');
        $package->save();

        return redirect()->route('manage.package.index');
    }

    public function edit(Package $package) {
        return view('manage.package.edit', [
            "package" => $package,
        ]);
    }

    public function update(PackageEditRequest $request, Package $package) {
        $package->name = $request->get('name');
        $package->service_id = $request->get('service_id');
        $package->save();

        return redirect()->route('manage.package.index');
    }

    public function destroy(Package $package) {
        $package->delete();

        return redirect()->back();
    }

    public function destroyMultiple(PackageDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $package = Package::findOrFail($selected);
            $package->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
