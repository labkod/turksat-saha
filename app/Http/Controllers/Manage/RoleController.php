<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\RoleCreateRequest;
use App\Http\Requests\Manage\RoleDestroyMultipleRequest;
use App\Http\Requests\Manage\RoleUpdateRequest;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::orderBy('name', 'ASC')->latest()->paginate(10);

        return view('manage.role.index', [
            "roles" => $roles,
        ]);
    }

    public function create()
    {
        return view('manage.role.create');
    }

    public function store(RoleCreateRequest $request)
    {
        $role = new Role;
        $role->name = str_slug($request->get('name'));
        $role->display_name = $request->get('name');
        $role->save();

        $role->users()->attach($request->get('users'));
        $role->attachPermissions($request->get('permissions'));

        return redirect()->route('manage.role.index');
    }

    public function edit(Role $role) {
        return view('manage.role.edit', [
            "role" => $role,
        ]);
    }

    public function update(RoleUpdateRequest $request, Role $role) {
        $role->name = str_slug($request->get('name'));
        $role->display_name = $request->get('name');
        $role->save();

        $role->users()->sync($request->get('users'));
        $role->perms()->sync($request->get('permissions'));

        return redirect()->route('manage.role.index');
    }

    public function destroy(Role $role) {
        $role->delete();

        return redirect()->back();
    }

    public function destroyMultiple(RoleDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $role = Role::findOrFail($selected);
            $role->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
