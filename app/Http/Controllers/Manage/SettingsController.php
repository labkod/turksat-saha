<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\SettingsUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function edit()
    {
        return view('manage.settings.edit');
    }

    public function update(SettingsUpdateRequest $request)
    {
        setting([
            'app_name' => $request->get('app_name'),
            'action_lock' => $request->get('action_lock'),
        ]);
        setting()->save();

        return redirect()->back();
    }
}
