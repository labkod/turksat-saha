<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\StreetCreateRequest;
use App\Http\Requests\Manage\StreetDestroyMultipleRequest;
use App\Http\Requests\Manage\StreetEditRequest;
use App\Http\Requests\Manage\TownDestroyMultipleRequest;
use App\Street;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StreetController extends Controller
{
    public function index(Town $town)
    {
        $streets = $town->streets()->orderBy("name", "ASC")->paginate(30);
        return view('manage.street.index', [
            "town"    => $town,
            "streets" => $streets,
        ]);
    }

    public function store(StreetCreateRequest $request, Town $town)
    {
        $street          = new Street;
        $street->name    = $request->get('name');
        $street->town_id = $town->id;
        $street->save();

        return redirect()->route('manage.location.street.index', $town->id);
    }

    public function edit(Town $town, Street $street)
    {
        return view('manage.street.edit', [
            "town"   => $town,
            "street" => $street,
        ]);
    }

    public function update(StreetEditRequest $request, Town $town, Street $street)
    {
        $street->name = $request->get('name');
        $street->save();

        return redirect()->route('manage.location.street.index', $town->id);
    }

    public function destroy(Town $town, Street $street)
    {
        $street->delete();

        return redirect()->back();
    }

    public function destroyMultiple(StreetDestroyMultipleRequest $request, Town $town)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $s = Street::findOrFail($selected);
            $s->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
