<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\TownCreateRequest;
use App\Http\Requests\Manage\TownDestroyMultipleRequest;
use App\Http\Requests\Manage\TownEditRequest;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TownController extends Controller
{
    public function index()
    {
        $towns = Town::orderBy("name", "ASC")->paginate(16);
        return view('manage.town.index', [
            "towns" => $towns,
        ]);
    }

    public function store(TownCreateRequest $request)
    {
        $town = new Town;
        $town->name = $request->get('name');
        $town->save();

        return redirect()->route('manage.location.index');
    }

    public function edit(Town $town)
    {
        return view('manage.town.edit', [
            "town" => $town,
        ]);
    }

    public function update(TownEditRequest $request, Town $town)
    {
        $town->name = $request->get('name');
        $town->save();

        return redirect()->route('manage.location.index');
    }

    public function destroy(Town $town)
    {
        $town->delete();

        return redirect()->back();
    }

    public function destroyMultiple(TownDestroyMultipleRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $t = Town::findOrFail($selected);
            $t->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
