<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\UserCreateRequest;
use App\Http\Requests\Manage\UserEditRequest;
use App\Http\Requests\Manage\UserMultipleDestroyRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('is_owner', false)->latest()->paginate(20);
        return view('manage.user.index', [
            "users" => $users,
        ]);
    }

    public function create()
    {
        return view('manage.user.create');
    }

    public function store(UserCreateRequest $request)
    {
        $user = new User;
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->save();

        $user->roles()->attach($request->get('roles'));
        $user->groups()->attach($request->get('groups'));

        return redirect()->route('manage.user.index');
    }

    public function edit(User $user)
    {
        return view('manage.user.edit', [
            "user" => $user,
        ]);
    }

    public function update(UserEditRequest $request, User $user)
    {
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        if($request->has('password'))
            $user->password = bcrypt($request->get('password'));
        $user->save();

        $user->roles()->sync($request->get('roles'));
        $user->groups()->sync($request->get('groups'));

        return redirect()->route('manage.user.index');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('manage.user.index');
    }

    public function destroyMultiple(UserMultipleDestroyRequest $request)
    {
        $result = [];

        foreach($request->get('select') as $selected){
            $user = User::findOrFail($selected);
            $user->delete();
        }

        $result["status"] = true;

        return $result;
    }
}
