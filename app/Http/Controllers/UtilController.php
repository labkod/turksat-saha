<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Campaign;
use App\District;
use App\Http\Requests\Util\DistrictsRequest;
use App\Http\Requests\Util\StreetsRequest;
use App\Street;
use App\Town;
use Illuminate\Http\Request;

class UtilController extends Controller
{

    public function streets(StreetsRequest $request) {
        $town = Town::find($request->get('town_id'));
        return $town->streets()->select('id', 'name')->get();
    }

    public function districts(DistrictsRequest $request) {
        $street = Street::find($request->get('street_id'));
        return $street->districts()->select('id', 'name')->get();
    }

    public function apartments(Request $request) {
        $district = District::find($request->get('district_id'));
        return $district->apartments()->select('id', 'name')->get();
    }

    public function homes(Request $request) {
        $result = [];
        $apartment = Apartment::find($request->get('apartment_id'));

        foreach($apartment->homes as $home)
            $result[] = [
                "id" => $home->id,
                "name" => $home->home_no." (".$home->name." ". $home->surname.")"
            ];

        return $result;
    }

    public function campaign(Request $request){
        $campaign = Campaign::find($request->get('campaign_id'));

        $result = [];

        foreach($campaign->packages as $package){
            $result[$package->service->name][$package->id] = $package->name;
        }

        return $result;
    }

}
