<?php

namespace App\Http\Requests\Action;

use Illuminate\Foundation\Http\FormRequest;

class ApartmentCheckEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "notes" => "required",
        ];

        if($this->has('option')){
            foreach($this->get('option') as $key => $opt){
                $rules["option.".$key] = "exists:check_options,id";
            }
        }

        if($this->has('removes')){
            foreach($this->get('removes') as $key => $opt){
                $rules["removes.".$key] = "exists:apartment_check_pictures,id";
            }
        }

        if($this->has('pictures')){
            foreach($this->file('pictures') as $key => $pic){
                $rules["pictures.".$key] = "image|max:6000";
            }
        }

        return $rules;
    }
}
