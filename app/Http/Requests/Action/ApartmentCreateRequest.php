<?php

namespace App\Http\Requests\Action;

use App\Street;
use App\Town;
use Illuminate\Foundation\Http\FormRequest;

class ApartmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required",
        ];

        if($this->get('town_id') != "not") {
            $rules["town_id"] = "required|exists:towns,id";

            if($this->get('street_id') != "not") {
                $rules["street_id"] = "required|exists:streets,id";

                if($this->get('district_id') != "not"){
                    $rules["district_id"] = "required|exists:districts,id";
                } else {
                    $street = Street::find($this->get('street_id'));
                    if($street->districts->count() > 0){
                        $rules["district_id"] = "required|not_in:not";
                    }
                }
            } else {
                $town = Town::find($this->get('town_id'));
                if($town->streets->count() > 0){
                    $rules["street_id"] = "required|not_in:not";
                }
            }
        } else {
            if(Town::count() > 0){
                $rules["town_id"] = "required|not_in:not";
            }
        }

        return $rules;
    }
}
