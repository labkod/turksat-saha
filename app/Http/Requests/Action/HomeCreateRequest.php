<?php

namespace App\Http\Requests\Action;

use Illuminate\Foundation\Http\FormRequest;

class HomeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "apartment_id" => "required|exists:apartments,id",
            "home_no"      => "required",
            "name"         => "required",
            "surname"      => "required",
            "identity"     => "required",
        ];

        if($this->get('provider_id') != 0)
            $rules["provider_id"] = "exists:providers,id";

        if($this->has('pictures')) {
            foreach ($this->file('pictures') as $key => $pic) {
                $rules["pictures." . $key] = "required|image|max:6000";
            }
        }

        return $rules;
    }
}
