<?php

namespace App\Http\Requests\Manage;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username" => "required|unique:users,username,".$this->route()->parameter('user')->id,
            "email"    => "nullable|email|unique:users,email,".$this->route()->parameter('user')->id,
            "password" => "nullable|min:6|confirmed",
        ];
    }
}
