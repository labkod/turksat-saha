<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

}
