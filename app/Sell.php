<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{

    public function home()
    {
        return $this->belongsTo('App\Home');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Package');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getIsLockedAttribute()
    {
        $created = $this->created_at;
        $now     = Carbon::now();

        return $now->diffInMinutes($created) > setting('action_lock');
    }

}
