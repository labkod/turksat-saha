<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    public function town()
    {
        return $this->belongsTo('App\Town');
    }

    public function districts()
    {
        return $this->hasMany('App\District');
    }

    public function apartments()
    {
        return $this->hasMany('App\Apartment');
    }
}
