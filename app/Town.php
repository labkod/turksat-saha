<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    public function streets()
    {
        return $this->hasMany('App\Street');
    }

    public function apartments()
    {
        return $this->hasMany('App\Apartment');
    }
}
