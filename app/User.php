<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDisplayNameAttribute() {
        return $this->name ? $this->name : $this->username;
    }

    public function apartment_checks() {
        return $this->hasMany('App\ApartmentCheck');
    }

    public function homes() {
        return $this->hasMany('App\Home');
    }

    public function sells() {
        return $this->hasMany('App\Sell');
    }

    public function groups() {
        return $this->belongsToMany('App\UserGroup')->withPivot('is_admin');
    }

    public function groupAdmins() {
        return $this->belongsToMany('App\UserGroup')->wherePivot('is_admin', true);
    }
}
