<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function admins()
    {
        return $this->belongsToMany('App\User')->wherePivot('is_admin', true);
    }
}
