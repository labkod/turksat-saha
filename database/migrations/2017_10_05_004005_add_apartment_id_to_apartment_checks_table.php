<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApartmentIdToApartmentChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_checks', function (Blueprint $table) {

            $table->integer('apartment_id')->unsigned();
            $table->foreign('apartment_id')
                  ->references('id')->on('apartments')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_checks', function (Blueprint $table) {

            $table->dropForeign("apartment_checks_apartment_id_foreign");
            $table->dropColumn('apartment_id');

        });
    }
}
