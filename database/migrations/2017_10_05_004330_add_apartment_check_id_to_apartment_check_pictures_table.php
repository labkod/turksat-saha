<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApartmentCheckIdToApartmentCheckPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_check_pictures', function (Blueprint $table) {

            $table->integer('apartment_check_id')->unsigned();
            $table->foreign('apartment_check_id')
                  ->references('id')->on('apartment_checks')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_check_pictures', function (Blueprint $table) {

            $table->dropForeign('apartment_check_pictures_apartment_check_id_foreign');
            $table->dropColumn('apartment_check_id');

        });
    }
}
