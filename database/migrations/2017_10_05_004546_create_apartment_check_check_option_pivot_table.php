<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentCheckCheckOptionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_check_check_option', function (Blueprint $table) {
            $table->integer('apartment_check_id')->unsigned()->index();
            $table->foreign('apartment_check_id')->references('id')->on('apartment_checks')->onDelete('cascade');

            $table->integer('check_option_id')->unsigned()->index();
            $table->foreign('check_option_id')->references('id')->on('check_options')->onDelete('cascade');

            $table->primary(['apartment_check_id', 'check_option_id'],"acco_aci_coi_primary");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartment_check_check_option');
    }
}
