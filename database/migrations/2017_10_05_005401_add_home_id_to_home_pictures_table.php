<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHomeIdToHomePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_pictures', function (Blueprint $table) {
            $table->integer('home_id')->unsigned();

            $table->foreign('home_id')
                  ->references('id')->on('homes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_pictures', function (Blueprint $table) {
            $table->dropForeign('home_pictures_home_id_foreign');
            $table->dropColumn('home_id');
        });
    }
}
