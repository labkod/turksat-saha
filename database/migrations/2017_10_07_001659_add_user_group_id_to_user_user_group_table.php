<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserGroupIdToUserUserGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_user_group', function (Blueprint $table) {
            $table->integer('user_group_id')->unsigned();
            $table->foreign('user_group_id')
                  ->references('id')->on('user_groups')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_user_group', function (Blueprint $table) {
            $table->dropForeign('user_user_group_user_group_id_foreign');
            $table->dropColumn('user_group_id');
        });
    }
}
