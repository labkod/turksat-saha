<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageIdToPackageSellTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_sell', function (Blueprint $table) {
            $table->integer('package_id')->unsigned();
            $table->foreign('package_id')
                  ->references('id')->on('packages')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_sell', function (Blueprint $table) {
            $table->dropForeign('package_sell_package_id_foreign');
            $table->dropColumn('package_id');
        });
    }
}
