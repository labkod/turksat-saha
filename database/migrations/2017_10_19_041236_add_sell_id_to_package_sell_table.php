<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellIdToPackageSellTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_sell', function (Blueprint $table) {
            $table->integer('sell_id')->unsigned();
            $table->foreign('sell_id')
                  ->references('id')->on('sells')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_sell', function (Blueprint $table) {
            $table->dropForeign('package_sell_sell_id_foreign');
            $table->dropColumn('sell_id');
        });
    }
}
