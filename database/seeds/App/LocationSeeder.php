<?php

use Illuminate\Database\Seeder;
use App\Town;
use App\Street;
use App\District;
use App\Apartment;

class LocationSeeder extends Seeder
{
    protected $locationPath;

    protected function getTotalData($json) {
        $total = 0;

        foreach($json as $val) {
            foreach($val as $val2) {
                foreach($val2 as $val3) {
                    $total += count($val3);
                    $total++;
                }
                $total++;
            }
            $total++;
        }

        return $total;
    }

    protected function createTown($name) {
        $town = new Town;
        $town->name = $name;
        $town->save();

        return $town;
    }

    protected function createStreet($name, Town $town) {
        $street = new Street;
        $street->town_id = $town->id;
        $street->name = $name;
        $street->save();

        return $street;
    }

    protected function createDistrict($name, Street $street) {
        $district = new District;
        $district->street_id = $street->id;
        $district->name = $name;
        $district->save();

        return $district;
    }

    protected function createApartment($name, Town $town, Street $street, District $district) {
        $apartment = new Apartment;
        $apartment->name = $name;
        $apartment->town_id = $town->id;
        $apartment->street_id = $street->id;
        $apartment->district_id = $district->id;
        $apartment->save();

        return $apartment;
    }

    public function run()
    {
        $this->locationPath = storage_path('result.json');

        $locations = json_decode(file_get_contents($this->locationPath));
        $totalData = $this->getTotalData($locations);
        $bar = $this->command->getOutput()->createProgressBar($totalData);
        foreach($locations as $town => $streets) {
            $townModel = $this->createTown($town);
            foreach($streets as $street => $districts) {
                $streetModel = $this->createStreet($street, $townModel);
                foreach($districts as $district => $apartments) {
                    $districtModel = $this->createDistrict($district, $streetModel);
                    foreach($apartments as $apartment) {
                        $this->createApartment($apartment, $townModel, $streetModel, $districtModel);
                        $bar->advance();
                    }
                    $bar->advance();
                }
                $bar->advance();
            }
            $bar->advance();
        }
        $bar->finish();

        $this->command->info(PHP_EOL.$totalData." locations created!");
    }
}
