var Action = function() {

    return {

        initValues: function(){
            this.districtBox = $("#district_id");
            this.apartmentSection = "#apartmentSection";
            this.housesSection = "#housesSection";
            this.houseCheckSection = "#houseCheckSection";
            this.houseNo=0;
        },

        loadingSection: function(section){
            $(section).html("<p class='text-center' style='margin-top: 20px;'><i class='fa fa-spin fa-spinner fa-3x'></i></p>");
        },

        handleDistrict: function(){
            var module = this;
            this.districtBox.change(function(){
                var districtId = parseInt($(this).val());
                if(districtId !== 0){
                    module.loadingSection(module.apartmentSection);

                    var source   = $("#apartment-template").html();
                    var template = Handlebars.compile(source);
                    var context  = {};
                    var html     = template(context);
                    $(module.apartmentSection).html(html);

                    $("#apartment_id").select2({
                        tags: true,
                        placeholder: "Apartman İsmi Giriniz",
                        width: null,
                        ajax: {
                            url: '/action/util/apartments',
                            data: function(params){
                                var results = {
                                    search: params.term,
                                    district_id: module.districtBox.val()
                                };
                                return results;
                            },
                            dataType: 'json'
                        }
                    });

                    module.handleApartment();

                } else {
                    $(module.apartmentSection).html('<div class="alert alert-warning">Cadde/Sokak Seçiniz</div>');
                }
            });
        },

        handleApartment: function(){
            var module = this;
            $('#apartment_id').on("select2:selecting", function(e) {
                var selected = e.params.args.data;
                if(selected.id !== selected.text) {
                    $("#apartmentDetail").hide();
                } else {
                    $("#apartmentDetail").show();
                }

                $(module.housesSection).html("Yükleniyor...");
                $(module.houseCheckSection).html("Yükleniyor...");
                $.ajax({
                    url: '/action/util/options',
                    type: 'GET',
                    dataType: 'JSON'
                }).done(function(data){
                    var source   = $("#houses-template").html();
                    var template = Handlebars.compile(source);
                    var html     = template({});
                    $(module.housesSection).html(html);

                    var acSource = $("#apartment-check-template").html();
                    var acTemplate = Handlebars.compile(acSource);
                    var acHtml = acTemplate({
                        'options': data
                    });
                    $(module.houseCheckSection).html(acHtml);

                    module.handleAddHouse();
                });
            });
        },

        handleAddHouse: function() {
            var module = this;
            $("#addHouse").bind('click', function(){
                module.houseNo+=1;
                var source   = $("#house-template").html();
                var template = Handlebars.compile(source);
                var newHouse = template({
                    'no': module.houseNo
                });
                $("#houses").append(newHouse);
                module.handleSell();
            });
        },

        handleSell: function() {
            this.handleAddSell();
        },

        handleAddSell: function() {
            var module = this;
            $(".button-open-sell").bind('click', function(){
                var no = $(this).data('no');
                $("[name='home["+no+"][sell]']").val("1");
                console.log('opening...');
                var target = $(this).data('target');
                $(target).show();
                module.handleCampaign();
            });
        },

        handleCampaign: function() {
            $(".campaign-select").bind('click', function(){
                var no = $(this).data('no');
                var campaignArea = $("#home-"+no+"-campaignArea");
                var nonCampaignArea = $("#home-"+no+"-nonCampaignArea");
                if($(this).val() == "0"){
                    nonCampaignArea.show();
                    campaignArea.hide();
                } else {
                    campaignArea.show();
                    nonCampaignArea.hide();

                    campaignArea.html("<p>Yükleniyor...</p>");
                    $.ajax({
                        url: $(this).data('url'),
                        method: 'GET',
                        data: 'campaign_id='+$(this).val(),
                        dataType: 'JSON'
                    }).done(function(data){
                        campaignArea.html("");

                        $.each(data, function(service, packages){
                            var row = "<div class='row'>";
                            row += "<div class='col-sm-12'><h4 style='border-bottom: 1px solid silver; padding-bottom: 5px;'>"+service+"</h4></div>";
                            $.each(packages, function(packageId, packageName){
                                row += "<div class='col-sm-4'>";
                                row += "<div class='form-group'>";
                                row += "<label>";
                                row += "<input type='checkbox' name='home["+no+"][campaignPackages]["+packageId+"]' value='"+packageId+"'>";
                                row += packageName;
                                row += "</label>";
                                row += "</div>";
                                row += "</div>";
                            });
                            row += "</div>";

                            campaignArea.append(row);
                        });
                    });
                }
            });
        },

        resetAction: function(){
            $(this.apartmentSection).html('<div class="alert alert-warning">Cadde/Sokak Seçiniz</div>');
            $(this.housesSection).html('<div class="alert alert-warning">Cadde/Sokak Seçiniz</div>');
            $(this.houseCheckSection).html('<div class="alert alert-warning">Cadde/Sokak Seçiniz</div>');
        },

        handleOthers: function() {
            var module = this;
            $("#town_id").change(function(){
                module.resetAction();
            });
            $("#street_id").change(function(){
                module.resetAction();
            });
            $("#district_id").change(function(){
                module.resetAction();
            });
        },

        init: function() {

            this.initValues();
            this.handleOthers();
            this.handleDistrict();

        }
    };

}();

jQuery(document).ready(function() {
    Action.init(); // initialize 'Action' module
});
