var LocationWizard = function() {

    return {

        initRoutes: function() {
            this.streetsRoute = $('meta[name="streets-route"]').attr('content');
            this.districtsRoute = $('meta[name="districts-route"]').attr('content');
            this.apartmentsRoute = $('meta[name="apartments-route"]').attr('content');
            this.homesRoute      = $('meta[name="homes-route"]').attr('content');
        },

        initElements: function() {
            this.towns     = $("[name='town_id']");
            this.streets   = $("[name='street_id']");
            this.districts = $("[name='district_id']");
            this.apartments = $("[name='apartment_id']");
            this.homes      = $("[name='home_id']");
        },

        disableAll: function() {
            this.towns.attr('disabled', 'disabled');
            this.streets.attr('disabled', 'disabled');
            this.districts.attr('disabled', 'disabled');
            this.apartments.attr('disabled', 'disabled');
            this.homes.attr('disabled', 'disabled');
        },

        enableAll: function() {
            this.towns.removeAttr('disabled');
            this.streets.removeAttr('disabled');
            this.districts.removeAttr('disabled');
            this.apartments.removeAttr('disabled');
            this.homes.removeAttr('disabled');
        },

        loadAnim: function(elements) {
            elements.html("<option>Yükleniyor...</option>");
        },

        removeAnim: function(elements, text) {
            elements.html("<option>"+text+"</option>");
        },

        selectDefault: function(elements) {
            elements.val(0);
        },

        setContent: function(element, data, val, otherEl) {
            element.html("");

            if(data.length > 0){

                if(val){
                    element.append("<option value='0'>"+val+"</option>");
                }

                $.each(data, function(i, content){
                    element.append("<option value='"+content.id+"'>"+content.name+"</option>");
                });
            } else {
                element.html("<option value='not'>Kayıt Bulunamadı</option>");
                if(otherEl.length > 0) {
                    otherEl.html("<option value='not'>Kayıt Bulunamadı</option>");
                }
            }
        },

        handleTown: function() {
            var module = this;
            module.towns.change(function(){
                module.disableAll();
                module.loadAnim(module.streets);
                module.loadAnim(module.districts);
                module.loadAnim(module.apartments);
                module.loadAnim(module.homes);

                var townId = $(this).val();

                if(townId == 0) {
                    module.removeAnim(module.streets, "Önce İlçe Seçiniz");
                    module.removeAnim(module.districts, "Önce İlçe Seçiniz");
                    module.removeAnim(module.apartments, "Önce İlçe Seçiniz");
                    module.removeAnim(module.homes, "Önce İlçe Seçiniz");
                    module.enableAll();
                } else {
                    $.ajax({
                        url: module.streetsRoute,
                        data: "town_id="+townId,
                        dataType: 'JSON',
                        method: 'GET'
                    }).done(function(data){
                        module.enableAll();
                        module.removeAnim(module.apartments, "Önce Mahalle Seçiniz");
                        module.removeAnim(module.districts, "Önce Mahalle Seçiniz");
                        module.removeAnim(module.homes, "Önce Mahalle Seçiniz");
                        module.setContent(module.streets, data, "Mahalle Seçiniz", module.districts);
                    }).fail(function(jqXHR){
                        if(jqXHR.responseJSON.errors){
                            var errors = "";
                            $.each(jqXHR.responseJSON.errors, function(i, errs){
                                $.each(errs, function(x, error){
                                    errors += error+"\n";
                                });
                            });
                            alert(errors);
                        } else {
                            alert(jqXHR.statusText);
                        }
                        module.selectDefault(module.towns);
                        module.removeAnim(module.streets, "Önce İlçe Seçiniz");
                        module.removeAnim(module.districts, "Önce İlçe Seçiniz");
                        module.removeAnim(module.apartments, "Önce İlçe Seçiniz");
                        module.removeAnim(module.homes, "Önce İlçe Seçiniz");
                        module.enableAll();
                    });
                }
            });
        },

        handleStreet: function() {
            var module = this;
            module.streets.change(function(){
                module.disableAll();
                module.loadAnim(module.districts);
                module.loadAnim(module.apartments);
                module.loadAnim(module.homes);

                var streetId = $(this).val();

                if(streetId == 0) {
                    module.removeAnim(module.districts, "Önce Mahalle Seçiniz");
                    module.removeAnim(module.apartments, "Önce Mahalle Seçiniz");
                    module.removeAnim(module.homes, "Önce Mahalle Seçiniz");
                    module.enableAll();
                } else {
                    $.ajax({
                        url: module.districtsRoute,
                        data: "street_id="+streetId,
                        dataType: 'JSON',
                        method: 'GET'
                    }).done(function(data){
                        module.enableAll();
                        module.setContent(module.districts, data, "Cadde/Sokak Seçiniz");
                        module.removeAnim(module.apartments, "Önce Cadde/Sokak Seçiniz");
                        module.removeAnim(module.homes, "Önce Cadde/Sokak Seçiniz");
                    }).fail(function(jqXHR){
                        if(jqXHR.responseJSON.errors){
                            var errors = "";
                            $.each(jqXHR.responseJSON.errors, function(i, errs){
                                $.each(errs, function(x, error){
                                    errors += error+"\n";
                                });
                            });
                            alert(errors);
                        } else {
                            alert(jqXHR.statusText);
                        }
                        module.selectDefault(module.streets);
                        module.removeAnim(module.districts, "Önce Mahalle Seçiniz");
                        module.removeAnim(module.apartments, "Önce Mahalle Seçiniz");
                        module.removeAnim(module.homes, "Önce Mahalle Seçiniz");
                        module.enableAll();
                    });
                }
            });
        },

        handleDistrict: function() {
            var module = this;
            module.districts.change(function(){
                module.disableAll();
                module.loadAnim(module.apartments);
                module.loadAnim(module.homes);

                var districtId = $(this).val();

                if(districtId == 0) {
                    module.removeAnim(module.apartments, "Önce Cadde/Sokak Seçiniz");
                    module.removeAnim(module.homes, "Önce Cadde/Sokak Seçiniz");
                    module.enableAll();
                } else {
                    $.ajax({
                        url: module.apartmentsRoute,
                        data: "district_id="+districtId,
                        dataType: 'JSON',
                        method: 'GET'
                    }).done(function(data){
                        module.enableAll();
                        module.setContent(module.apartments, data, "Apartman Seçiniz");
                        module.removeAnim(module.homes, "Önce Apartman Seçiniz")
                    }).fail(function(jqXHR){
                        if(jqXHR.responseJSON.errors){
                            var errors = "";
                            $.each(jqXHR.responseJSON.errors, function(i, errs){
                                $.each(errs, function(x, error){
                                    errors += error+"\n";
                                });
                            });
                            alert(errors);
                        } else {
                            alert(jqXHR.statusText);
                        }
                        module.selectDefault(module.districts);
                        module.removeAnim(module.apartments, "Önce Cadde/Sokak Seçiniz");
                        module.removeAnim(module.homes, "Önce Cadde/Sokak Seçiniz");
                        module.enableAll();
                    });
                }
            });
        },

        handleApartment: function() {
            var module = this;
            module.apartments.change(function(){
                module.disableAll();
                module.loadAnim(module.homes);

                var apartmentId = $(this).val();

                if(apartmentId == 0) {
                    module.removeAnim(module.homes, "Önce Apartman Seçiniz");
                    module.enableAll();
                } else {
                    $.ajax({
                        url: module.homesRoute,
                        data: "apartment_id="+apartmentId,
                        dataType: 'JSON',
                        method: 'GET'
                    }).done(function(data){
                        module.enableAll();
                        module.setContent(module.homes, data, "Daire Seçiniz");
                    }).fail(function(jqXHR){
                        if(jqXHR.responseJSON.errors){
                            var errors = "";
                            $.each(jqXHR.responseJSON.errors, function(i, errs){
                                $.each(errs, function(x, error){
                                    errors += error+"\n";
                                });
                            });
                            alert(errors);
                        } else {
                            alert(jqXHR.statusText);
                        }
                        module.selectDefault(module.apartments);
                        module.removeAnim(module.homes, "Önce Apartman Seçiniz");
                        module.enableAll();
                    });
                }
            });
        },

        init: function() {

            this.initRoutes();
            this.initElements();
            this.handleTown();
            this.handleStreet();
            this.handleDistrict();
            this.handleApartment();

        }
    };

}();

jQuery(document).ready(function() {
    LocationWizard.init(); // initialize 'LocationWizard' module
});
