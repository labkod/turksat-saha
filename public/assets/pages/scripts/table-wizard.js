var TableWizard = function() {

    return {

        initTable: function() {

        },

        handleDelete: function() {
            $(".btn-delete-all").bind('click', function(){

                if($(".table-select:checked").length > 0){
                    var button = $(this);
                    var oldHtmlOnButton = button.html();
                    $(this).html('<i class="fa fa-spinner fa-spin"></i>');

                    var url = button.data('url');
                    var send = "_method=DELETE";

                    $(".table-select:checked").each(function(i, select){
                        send+= "&select[]="+$(select).val();
                    });

                    $.ajax({
                        url: url,
                        method: "POST",
                        data: send,
                        dataType: "JSON"
                    }).done(function(data){
                        if(data.status) {
                            window.location.reload();
                        } else {
                            alert(data.error);
                        }
                    }).fail(function(jqXHR){
                        if(jqXHR.status === 422){

                            var errors = [];
                            $.each(jqXHR.responseJSON.errors, function(i, err){
                                errors.push(err);
                            });

                            alert(errors.join('\n'));

                        } else {
                            alert(jqXHR.statusText);
                        }
                    }).complete(function(){
                        button.html(oldHtmlOnButton);
                    });
                } else {
                    alert("Lütfen silme işlemi için bir veya daha fazla nesne seçiniz.");
                }
            });
        },

        handleSelectAll: function() {

            $(".table-select-all").bind('click', function(){
                var table  = $(this).closest('table');
                var toggle = $(this).prop('checked');

                $('.table-select', table).each(function(i, checkbox){
                    $(checkbox).attr('checked', toggle);

                    if(toggle){
                        $(checkbox).parent().addClass('checked');
                    } else {
                        $(checkbox).parent().removeClass('checked');
                    }
                });
            });

        },

        handleDeleteConfirm: function() {
            $(".delete-confirm").bind('click', function(event){
                var actionUrl = $(this).attr('href');
                bootbox.confirm({
                    size: "small",
                    message: "Silmek istediğinizden emin misiniz?",
                    callback: function(result){
                        if(result) {
                            location.href = actionUrl;
                        }
                    }
                });
                event.preventDefault();
            });
        },

        init: function() {

            this.initTable();
            this.handleDelete();
            this.handleSelectAll();
            this.handleDeleteConfirm();

        }
    };

}();

jQuery(document).ready(function() {
    TableWizard.init(); // initialize 'TableWizard' module
});
