@extends('layout.page')

@section('title', "Hızlı İşlem Girişi")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Hızlı İşlem Girişi</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Hızlı İşlem Girişi</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.store"], "method" => "POST", "files" => true]) !!}

                    @include("util.error")

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('town_id', "İlçe", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    {!! Form::select('town_id', [0 => "İlçe Seçiniz"] + \App\Town::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), old('town_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('street_id', "Mahalle", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $streets = [0 => "Önce İlçe Seçiniz"];

                                    if(old('town_id')){
                                        $town = \App\Town::find(old('town_id'));
                                        if($town){
                                            $streets = [0 => "Mahalle Seçiniz"] + $town->streets()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('street_id', $streets, old('town_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('district_id', "Cadde/Sokak", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $districts = [0 => "Önce İlçe Seçiniz"];

                                    if(old('street_id') && isset($town)){
                                        $street = \App\Street::find(old('street_id'));
                                        if($street){
                                            $districts = [0 => "Cadde/Sokak Seçiniz"] + $street->districts()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('district_id', $districts, old('district_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="portlet green-sharp box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-building"></i> Apartman
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body" id="apartmentSection">
                                <div class="alert alert-warning no-margin">
                                    Cadde/Sokak Seçiniz
                                </div>
                            </div>
                        </div>

                        <div class="portlet purple-sharp box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-building"></i> Apartman Girişi
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body" id="houseCheckSection">
                                <div class="alert alert-warning no-margin">
                                    Apartman Seçiniz
                                </div>
                            </div>
                        </div>

                        <div class="portlet blue-sharp box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-users"></i> Daireler
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body" id="housesSection">
                                <div class="alert alert-warning no-margin">
                                    Apartman Seçiniz
                                </div>
                            </div>
                        </div>

                    <div class="form-group text-center">
                        {!! Form::button('Tüm Girişleri Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->

    <script id="apartment-template" type="text/x-handlebars-template">
        <div class="form-group">
            <label for="apartment_id" class="control-label">Apartman Adı</label>
            <select name="apartment_id" id="apartment_id" class="form-control select2"></select>
        </div>
        <div id="apartmentDetail" style="display: none;">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('visor_name', "Yetkili Adı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            {!! Form::text('visor_name', old('visor_name'), ["class" => "form-control", "placeholder" => "Apartman yetkilisinin adını giriniz."]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('visor_surname', "Yetkili Soyadı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            {!! Form::text('visor_surname', old('visor_surname'), ["class" => "form-control", "placeholder" => "Apartman yetkilisinin soyadını giriniz."]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('visor_phone', "Yetkili Telefon", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-phone fa-fw"></i>
                            {!! Form::text('visor_phone', old('visor_phone'), ["type" => "tel", "class" => "form-control", "placeholder" => "Apartman yetkilisinin telefonunu giriniz."]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script id="apartment-check-template" type="text/x-handlebars-template">

        @{{#if options}}
        <h4 style="margin-top: 0;">Seçenekler</h4>
        @{{#each options}}
        <div class="form-group">
            <label>
                <input type="checkbox" name="apartment_check_option[@{{id}}]" value="@{{id}}" class="form-control" />
                @{{name}}
            </label>
        </div>
        @{{/each}}
        @{{/if}}

        <div class="form-group">
            <label for="apartment_check_pictures" class="control-label">Resimler</label>
            <div class="input-icon">
                <i class="fa fa-file fa-fw"></i>
                <input type="file" name="apartment_check_pictures[]" class="form-control" multiple />
            </div>
        </div>

        <div class="form-group">
            <label for="apartment_check_notes" class="control-label">Notlar</label>
            <div class="input-icon">
                <i class="fa fa-pencil fa-fw"></i>
                <textarea name="apartment_check_notes" class="form-control" placeholder="Apartman ile ilgili bilgi notu giriniz."></textarea>
            </div>
        </div>
    </script>

    <script id="houses-template" type="text/x-handlebars-template">
        <div id="houses">

        </div>
        <button type="button" id="addHouse" class="btn btn-primary btn-block"><i class="fa fa-user-plus"></i> Yeni Daire</button>
    </script>

    <script id="house-template" type="text/x-handlebars-template">
        <div id="home-@{{no}}">
            <input type="hidden" name="home[@{{no}}][sell]" value="0" />
            <div class="form-group">
                <label for="home-@{{no}}-home-no" class="control-label">Daire No</label>
                <div class="input-icon">
                    <i class="fa fa-building fa-fw"></i>
                    <input type="number" required name="home[@{{no}}][home_no]" id="home-@{{no}}-home-no" class="form-control" placeholder="Daire numarasını giriniz.">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="home-@{{no}}-name" class="control-label">Ad</label>
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            <input type="text" required name="home[@{{no}}][name]" class="form-control" placeholder="Daire sakininin adını giriniz." />
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="home-@{{no}}-surname" class="control-label">Soyad</label>
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            <input type="text" required name="home[@{{no}}][surname]" class="form-control" placeholder="Daire sakininin soyadını giriniz." />
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="home-@{{no}}-identity" class="control-label">T.C. Kimlik Numarası</label>
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            <input type="number" name="home[@{{no}}][identity]" class="form-control" placeholder="Daire sakininin kimlik numarasını giriniz." />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="home-@{{no}}-phone" class="control-label">Telefon Numarası</label>
                        <div class="input-icon">
                            <i class="fa fa-phone fa-fw"></i>
                            <input type="tel" name="home[@{{no}}][phone]" class="form-control" placeholder="İletişim telefon numarası" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="home-@{{no}}-email" class="control-label">E-Posta Adresi</label>
                        <div class="input-icon">
                            <i class="fa fa-envelope fa-fw"></i>
                            <input type="email" name="home[@{{no}}][email]" class="form-control" placeholder="İletişim e-posta adresi" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="home-@{{no}}-provider" class="control-label">Servis Sağlayıcı</label>
                        <div class="input-icon">
                            <i class="fa fa-globe fa-fw"></i>
                            <select name="home[@{{no}}][provider_id]" class="form-control">
                                <option value="0">Kullanmıyor</option>
                                @foreach(\App\Provider::all() as $provider)
                                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="home-@{{no}}-commitment" class="control-label">Taahhüt Bitiş Tarihi</label>
                        <div class="input-icon">
                            <i class="fa fa-calendar fa-fw"></i>
                            <input type="text" name="home[@{{no}}][commitment]" class="form-control" placeholder="Gün.Ay.Yıl" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="home-@{{no}}-pictures" class="control-label">Resimler</label>
                <div class="input-icon">
                    <i class="fa fa-file fa-fw"></i>
                    <input type="file" name="home[@{{no}}][pictures][]" class="form-control" multiple />
                </div>
            </div>

            <div class="form-group">
                <label for="home-@{{no}}-notes" class="control-label">Notlar</label>
                <div class="input-icon">
                    <i class="fa fa-pencil fa-fw"></i>
                    <textarea name="home[@{{no}}][notes]" class="form-control" placeholder="Daire ile ilgili bilgi notu giriniz."></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-danger">Bu Daireyi Sil <i class="fa fa-trash"></i></button>

                    <button type="button" class="btn btn-success button-open-sell" data-no="@{{no}}" data-target="#home@{{no}}sell">Daire için Satış Ekle <i class="fa fa-try"></i></button>
                </div>
            </div>

            <div id="home@{{no}}sell" style="display: none;">
                <h3>Satış</h3>
                <div class="form-group">
                    <label for="home-@{{no}}-campaign" class="control-label">Kampanya</label>
                    <div class="input-icon">
                        <i class="fa fa-star fa-fw"></i>
                        <select id="home-@{{no}}-campaign" data-no="@{{no}}" name="home[@{{no}}][campaign_id]" class="form-control campaign-select" data-url="{{ route('util.campaign') }}">
                            <option value="0">Kampanya Yok</option>
                            @foreach(\App\Campaign::orderBy('name', 'ASC')->get() as $campaign)
                                <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div id="home-@{{no}}-campaignArea">

                </div>

                <div id="home-@{{no}}-nonCampaignArea">
                    @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 style="border-bottom: 1px solid silver; padding-bottom: 5px;">{{ $service->name }}</h4>
                            </div>

                            @foreach($service->packages as $key => $package)
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="home[@{{no}}][packages][{{ $package->id }}]" value="{{ $package->id }}">
                                            {{ $package->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endforeach
                </div>
            </div>

            <hr>
        </div>
    </script>
@stop

@section('plugin.css')
    @parent
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/handlebars/handlebars-v4.0.10.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script>
        $.fn.select2.defaults.set("theme", "bootstrap");
    </script>
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
    <script src="{{ asset('assets/pages/scripts/action.js') }}"></script>
@stop
