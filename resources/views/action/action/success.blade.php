@extends('layout.page')

@section('title', "Hızlı İşlem Girişi Tamamlandı")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Hızlı İşlem Girişi Tamamlandı</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Başarılı</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="alert alert-success">
                        Hızlı işlem girişi başarılı bir şekilde tamamlandı.
                    </div>
                    @if($apartment)
                        <h3>Apartman Oluşturma: <a target="_blank" class="btn btn-primary" href="{{ route('actions.apartment.edit', [$apartment->id]) }}">{{ $apartment->name }} Girişi Eklendi</a></h3>
                    @endif

                    @if($apartmentCheck)
                        <h3>Apartman Girişi: <a target="_blank" class="btn btn-warning" href="{{ route('actions.apartment.check.show', [$apartmentCheck->apartment->id, $apartmentCheck->id]) }}">{{ $apartmentCheck->apartment->name }} (#{{$apartmentCheck->id}}) Kaydını Görüntüle</a></h3>
                    @endif

                    @if($homes)
                        <h3>Eklenen Daireler</h3>
                        <ul>
                            @foreach($homes as $home)
                            <li>Daire: <a class="btn btn-danger" target="_blank" href="{{ route('actions.home.show', $home->id) }}">{{ $home->home_no }} Dairesi Eklendi</a></li>
                            @endforeach
                        </ul>
                    @endif

                    @if($sells)
                        <h3>Eklenen Satışlar</h3>
                        <ul>
                            @foreach($sells as $sell)
                            <li>Satış: <a class="btn btn-danger" target="_blank" href="{{ route('actions.sell.show', $sell->id) }}">{{ $sell->home->home_no }} ({{ $sell->home->name }} {{ $sell->home->surname }}) Dairesi için Satış Eklendi</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.css')
    @parent
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('plugin.js')
    @parent
@stop

@section('page.js')
    @parent
    <script>
        $.fn.select2.defaults.set("theme", "bootstrap");
    </script>
@stop