@extends('layout.page')

@section('title', $apartment->name." - Giriş Oluşturma")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.index') }}">Apartmanlar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.check.index', $apartment->id) }}">{{ $apartment->name }} Girişleri</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yeni Giriş</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yeni Giriş Ekle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.apartment.check.store", $apartment->id], "method" => "POST", "files" => true]) !!}

                    @include("util.error")

                    <h4 style="margin-top: 0;">Seçenekler</h4>
                    @foreach(\App\CheckOption::all() as $option)
                        <div class="form-group">
                            <label>
                                {!! Form::checkbox('option['.$option->id.']', $option->id, old('option.'.$option->id), ["class" => "form-control"]) !!}
                                {{ $option->name }}
                            </label>
                        </div>
                    @endforeach

                    <div class="form-group">
                        {!! Form::label('pictures', "Resimler", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-file fa-fw"></i>
                            {!! Form::file('pictures[]', ["class" => "form-control", "multiple"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', "Notlar", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-pencil fa-fw"></i>
                            {!! Form::textarea('notes', old('notes'), ["class" => "form-control", "placeholder" => "Apartman ile ilgili bilgi notu giriniz."]) !!}
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Giriş Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop