@extends('layout.page')

@section('title', $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Giriş Düzenleme")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.index') }}">Apartmanlar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.check.index', $apartment->id) }}">{{ $apartment->name }} Girişleri</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Giriş Düzenleme" }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Giriş Düzenleme" }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.apartment.check.update", $apartment->id, $check->id], "method" => "PUT", "files" => true]) !!}

                    @include("util.error")

                    <h4 style="margin-top: 0;">Seçenekler</h4>
                    @foreach(\App\CheckOption::all() as $option)
                        <div class="form-group">
                            <label>
                                {!! Form::checkbox('option['.$option->id.']', $option->id, old('option.'.$option->id) ? old('option.'.$option->id) : $check->options()->where('id', $option->id)->count(), ["class" => "form-control"]) !!}
                                {{ $option->name }}
                            </label>
                        </div>
                    @endforeach

                    @if($check->pictures->count() > 0)
                        <h4>Önceki Fotoğraflar</h4>
                        <div class="row">
                            @foreach($check->pictures as $picture)
                                <div class="col-md-4">
                                    <label>
                                        {!! Form::checkbox('removes['.$picture->id.']', $picture->id, old('removes.'.$picture->id), ["class" => "form-control"]) !!}
                                        Kaldır
                                    </label>
                                    <a title="{{ $picture->name }}" href="{{ asset($picture->file) }}" target="_blank">
                                        <img src="{{ asset($picture->file) }}" class="img-thumbnail img-responsive" style="height: 220px; max-width: 100%;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="form-group">
                        {!! Form::label('pictures', "Yeni Fotoğraflar", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-file fa-fw"></i>
                            {!! Form::file('pictures[]', ["class" => "form-control", "multiple"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', "Notlar", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-pencil fa-fw"></i>
                            {!! Form::textarea('notes', old('notes') ? old('notes') : $check->notes, ["class" => "form-control", "placeholder" => "Apartman ile ilgili bilgi notu giriniz."]) !!}
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Giriş Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop