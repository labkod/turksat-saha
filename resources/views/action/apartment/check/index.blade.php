@extends('layout.page')

@section('title', $apartment->name." Apartmanı Girişleri")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.index') }}">Apartmanlar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $apartment->name }} Girişlerim</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $apartment->name }} Girişlerim</span>
                    </div>
                    <div class="actions">
                        @if($checks->count() > 0)
                            @permission('actions.apartment.check.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('actions.apartment.check.destroyMulti', $apartment->id) }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('actions.apartment.check.create')
                        <a class="btn green btn-outline" href="{{ route('actions.apartment.check.create', $apartment->id) }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Giriş Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($checks->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('actions.apartment.check.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th width="5%"> # </th>
                                    <th width="20%"> Giriş Tarihi </th>
                                    <th> Fotoğraf Sayısı </th>
                                    @if(auth()->user()->can('actions.apartment.check.edit') || auth()->user()->can('actions.apartment.check.delete') || auth()->user()->can('actions.apartment.check.show'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($checks as $i => $check)
                                    <tr>
                                        @permission('actions.apartment.check.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $check->id }}" class="table-select"></td>
                                        @endpermission
                                        <td> {{ $i+1 }} </td>
                                        <td>
                                            {{ $check->created_at->format("d.m.Y H:i") }}
                                        </td>
                                        <td>
                                            <span class="label label-info">
                                                <i class="fa fa-photo"></i>
                                                {{ $check->pictures->count() }}
                                                <span class="hidden-480">Fotoğraf</span>
                                            </span>
                                        </td>
                                        @if(auth()->user()->can('actions.apartment.check.edit') || auth()->user()->can('actions.apartment.check.delete') || auth()->user()->can('actions.apartment.check.show'))
                                        <td>
                                            @permission('actions.apartment.check.show')
                                            <a href="{{ route('actions.apartment.check.show', [$apartment->id, $check->id]) }}" class="btn blue btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-eye"></i> <span class="hidden-480">Görüntüle</span>
                                            </a>
                                            @endpermission

                                            @if(!$check->isLocked)
                                                @permission('actions.apartment.check.edit')
                                                <a href="{{ route('actions.apartment.check.edit', [$apartment->id, $check->id]) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                    <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                                </a>
                                                @endpermission

                                                @permission('actions.apartment.check.delete')
                                                <a href="{{ route('actions.apartment.check.destroy', [$apartment->id, $check->id]) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                    <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                                </a>
                                                @endpermission
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>
                                Henüz bir giriş oluşturulmadı.
                                @permission('actions.apartment.check.create')
                                Yeni bir <a href="{{ route('actions.apartment.check.create', $apartment->id) }}">giriş oluştur</a>abilirsiniz.
                                @endpermission
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop