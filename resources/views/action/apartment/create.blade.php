@extends('layout.page')

@section('title', "Yeni Apartman Oluşturma")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.index') }}">Apartmanlar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yeni Apartman</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yeni Apartman Ekle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.apartment.store"], "method" => "POST"]) !!}

                    @include("util.error")

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('town_id', "İlçe", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    {!! Form::select('town_id', [0 => "İlçe Seçiniz"] + \App\Town::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), old('town_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('street_id', "Mahalle", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $streets = [0 => "Önce İlçe Seçiniz"];

                                    if(old('town_id')){
                                        $town = \App\Town::find(old('town_id'));
                                        if($town){
                                            $streets = [0 => "Mahalle Seçiniz"] + $town->streets()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('street_id', $streets, old('town_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('district_id', "Cadde/Sokak", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $districts = [0 => "Önce İlçe Seçiniz"];

                                    if(old('street_id') && isset($town)){
                                        $street = \App\Street::find(old('street_id'));
                                        if($street){
                                            $districts = [0 => "Cadde/Sokak Seçiniz"] + $street->districts()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('district_id', $districts, old('district_id'), ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', "Apartman Adı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-building fa-fw"></i>
                            {!! Form::text('name', old('name'), ["class" => "form-control", "placeholder" => "Apartman için bir isim giriniz"]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('visor_name', "Yetkili Adı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('visor_name', old('visor_name'), ["class" => "form-control", "placeholder" => "Apartman yetkilisinin adını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('visor_surname', "Yetkili Soyadı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('visor_surname', old('visor_surname'), ["class" => "form-control", "placeholder" => "Apartman yetkilisinin soyadını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('visor_phone', "Yetkili Telefon", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-phone fa-fw"></i>
                                    {!! Form::text('visor_phone', old('visor_phone'), ["type" => "tel", "class" => "form-control", "placeholder" => "Apartman yetkilisinin telefonunu giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Apartman Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop