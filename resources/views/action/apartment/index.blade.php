@extends('layout.page')

@section('title', "Apartman Girişleri")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Apartmanlar</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Apartmanlar</span>
                    </div>
                    <div class="actions">
                        @if($apartments->count() > 0)
                            @permission('actions.apartment.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('actions.apartment.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('actions.apartment.create')
                        <a class="btn green btn-outline" href="{{ route('actions.apartment.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Apartman Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($apartments->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('actions.apartment.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th> İlçe / Mahalle </th>
                                    <th> Sokak </th>
                                    <th> Apartman Adı </th>
                                    <th> Son Giriş Tarihi </th>
                                    @permission('actions.apartment.check')
                                    <th> Girişleriniz </th>
                                    @endpermission
                                    @if(auth()->user()->can('actions.apartment.edit') || auth()->user()->can('actions.apartment.delete'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($apartments as $apartment)
                                    <tr>
                                        @permission('actions.apartment.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $apartment->id }}" class="table-select"></td>
                                        @endpermission
                                        <td>
                                            @if($apartment->attachedTown)
                                            <span class="label label-info">{{ $apartment->attachedTown->name }}</span>
                                            @endif

                                            @if($apartment->attachedStreet)
                                                /
                                            <span class="label label-info">{{ $apartment->attachedStreet->name }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($apartment->district)
                                            <span class="label label-info">{{ $apartment->district->name }}</span>
                                            @else
                                                <span class="label label-danger">Cadde/Sokak Girilmedi</span>
                                            @endif
                                        </td>
                                        <td>{{ $apartment->name }}</td>
                                        <td>
                                            @if($apartment->checks()->latest()->first())
                                            {{ $apartment->checks()->latest()->first()->created_at->format('d.m.Y H:i') }}
                                            @else
                                            <span class="label label-danger">Apartman Girişi Henüz Yapılmadı</span>
                                            @endif
                                        </td>
                                        @permission('actions.apartment.check')
                                        <td>
                                            <a href="{{ route('actions.apartment.check.index', $apartment->id) }}" class="btn btn-warning btn-xs">{{ $apartment->user_checks()->count() }} Giriş Yaptınız</a>
                                        </td>
                                        @endpermission
                                        @if(auth()->user()->can('actions.apartment.edit') || auth()->user()->can('actions.apartment.delete'))
                                        <td>
                                            @permission('actions.apartment.edit')
                                            <a href="{{ route('actions.apartment.edit', $apartment->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                            </a>
                                            @endpermission

                                            @permission('actions.apartment.delete')
                                            <a href="{{ route('actions.apartment.destroy', $apartment->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                            </a>
                                            @endpermission
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $apartments->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>
                                Henüz bir apartman oluşturulmadı.
                                @permission('actions.apartment.create')
                                Yeni bir <a href="{{ route('actions.apartment.create') }}">apartman oluştur</a>abilirsiniz.
                                @endpermission
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop