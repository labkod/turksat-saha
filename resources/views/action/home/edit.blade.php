@extends('layout.page')

@section('title', "Daire Girişi Düzenleme")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.home.index') }}">Daire Girişlerim</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Daire Girişi Düzenleme</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Daire Girişi Düzenleme</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.home.update", $home->id], "method" => "PUT", "files" => true]) !!}

                    @include("util.error")

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('apartment_id', "Apartman", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-building fa-fw"></i>
                                    {!! Form::select('apartment_id', [0 => "Seçiniz"] + \App\Apartment::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), $home->apartment->id, ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('home_no', "Daire No", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-building fa-fw"></i>
                                    {!! Form::text('home_no', $home->home_no, ["class" => "form-control", "placeholder" => "Daire numarasını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('name', "Ad", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('name', $home->name, ["class" => "form-control", "placeholder" => "Daire sakininin adını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('surname', "Soyad", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('surname', $home->surname, ["class" => "form-control", "placeholder" => "Daire sakininin soyadını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('identity', "T.C. Kimlik Numarası", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::number('identity', $home->identity, ["class" => "form-control", "placeholder" => "Daire sakininin kimlik numarasını giriniz."]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('phone', "Telefon Numarası", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-phone fa-fw"></i>
                                    {!! Form::text('phone', $home->phone, ["class" => "form-control", "placeholder" => "İletişim telefon numarası", "type" => "tel"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('email', "E-Posta Adresi", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-envelope fa-fw"></i>
                                    {!! Form::email('email', $home->email, ["class" => "form-control", "placeholder" => "İletişim e-posta adresi"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('provider_id', "Servis Sağlayıcı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-globe fa-fw"></i>
                                    {!! Form::select('provider_id', [0 => "Kullanmıyor"] + \App\Provider::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), old('provider_id') ? old('provider_id') : $home->provider ? $home->provider->id : "0", ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('commitment', "Taahhüt Bitiş Tarihi", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-calendar fa-fw"></i>
                                    {!! Form::text('commitment', old('commitment') ? old('commitment') : $home->humanCommitment, ["class" => "form-control", "placeholder" => "Gün.Ay.Yıl"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($home->pictures->count() > 0)
                        <h4>Önceki Fotoğraflar</h4>
                        <div class="row">
                            @foreach($home->pictures as $picture)
                                <div class="col-md-4">
                                    <label>
                                        {!! Form::checkbox('removes['.$picture->id.']', $picture->id, old('removes.'.$picture->id), ["class" => "form-control"]) !!}
                                        Kaldır
                                    </label>
                                    <a title="{{ $picture->name }}" href="{{ asset($picture->file) }}" target="_blank">
                                        <img src="{{ asset($picture->file) }}" class="img-thumbnail img-responsive" style="height: 220px; max-width: 100%;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="form-group">
                        {!! Form::label('pictures', "Yeni Fotoğraflar", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-file fa-fw"></i>
                            {!! Form::file('pictures[]', ["class" => "form-control", "multiple"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', "Notlar", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-pencil fa-fw"></i>
                            {!! Form::textarea('notes', $home->notes, ["class" => "form-control", "placeholder" => "Daire ile ilgili bilgi notu giriniz."]) !!}
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Girişi Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop