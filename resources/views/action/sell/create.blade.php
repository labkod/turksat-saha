@extends('layout.page')

@section('title', "Satış Girişi Oluşturma")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.sell.index') }}">Satış Girişlerim</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yeni Giriş</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yeni Giriş Ekle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["actions.sell.store"], "method" => "POST"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        {!! Form::label('home_id', "Daire", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-building fa-fw"></i>
                            {!! Form::select('home_id', $homes, old('home_id'), ["class" => "form-control"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('campaign_id', "Kampanya", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-star fa-fw"></i>
                            {!! Form::select('campaign_id', [0 => "Kampanya Yok"] + \App\Campaign::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), old('campaign_id'), ["class" => "form-control", "id" => "campaignSelect", "data-url" => route('util.campaign')]) !!}
                        </div>
                    </div>

                    <div id="campaignArea">

                    </div>

                    <div id="nonCampaignArea">
                        @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="border-bottom: 1px solid silver; padding-bottom: 5px;">{{ $service->name }}</h4>
                                </div>

                                @foreach($service->packages as $key => $package)
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>
                                                {!! Form::checkbox("packages[".$package->id."]", $package->id, old('packages.'.$package->id)) !!}
                                                {{ $package->name }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        @endforeach
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Giriş Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
    <script>

        var campaignArea = $("#campaignArea");
        var nonCampaignArea = $("#nonCampaignArea");

        campaignArea.hide();
        nonCampaignArea.show();

        $("#campaignSelect").change(function(){
            if($(this).val() == "0"){
                nonCampaignArea.show();
                campaignArea.hide();
            } else {
                campaignArea.show();
                nonCampaignArea.hide();

                campaignArea.html("<p>Yükleniyor...</p>");
                $.ajax({
                    url: $(this).data('url'),
                    method: 'GET',
                    data: 'campaign_id='+$(this).val(),
                    dataType: 'JSON'
                }).done(function(data){
                    campaignArea.html("");

                    $.each(data, function(service, packages){
                        var row = "<div class='row'>";
                        row += "<div class='col-sm-12'><h4 style='border-bottom: 1px solid silver; padding-bottom: 5px;'>"+service+"</h4></div>";
                        $.each(packages, function(packageId, packageName){
                            row += "<div class='col-sm-4'>";
                                row += "<div class='form-group'>";
                                    row += "<label>";
                                        row += "<input type='checkbox' name='campaignPackages["+packageId+"]' value='"+packageId+"'>";
                                        row += packageName;
                                    row += "</label>";
                                row += "</div>";
                            row += "</div>";
                        });
                        row += "</div>";

                        campaignArea.append(row);
                    });
                });
            }
        });
    </script>
@stop