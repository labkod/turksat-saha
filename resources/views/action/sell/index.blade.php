@extends('layout.page')

@section('title', "Satışlarım")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Satışlarım</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Satışlarım</span>
                    </div>
                    <div class="actions">
                        @if($sells->count() > 0)
                            @permission('actions.sell.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('actions.sell.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('actions.sell.create')
                        <a class="btn green btn-outline" href="{{ route('actions.sell.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Satış Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($sells->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('actions.sell.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th> İlçe / Mahalle / Sokak </th>
                                    <th> Apartman / Daire </th>
                                    <th> Ad Soyad </th>
                                    <th> Kampanya / Paket Sayısı</th>
                                    <th> Giriş Tarihi </th>
                                    @if(auth()->user()->can('actions.sll.edit') || auth()->user()->can('actions.sell.delete') || auth()->user()->can('action.sell.show'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sells as $sell)
                                    <tr>
                                        @permission('actions.sell.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $sell->id }}" class="table-select"></td>
                                        @endpermission
                                        <td>
                                            @if($sell->home->apartment->attachedTown)
                                            {{ $sell->home->apartment->attachedTown->name }}
                                            @endif

                                            @if($sell->home->apartment->attachedStreet)
                                                /
                                            {{ $sell->home->apartment->attachedStreet->name }}
                                            @endif

                                            @if($sell->home->apartment->district)
                                                /
                                                {{ $sell->home->apartment->district->name }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ $sell->home->apartment->name }} / {{ $sell->home->home_no }}
                                        </td>
                                        <td>{{ $sell->home->name." ".$sell->home->surname }}</td>
                                        <td>
                                            @if($sell->campaign)
                                                <span class="label label-primary">{{ $sell->campaign->name }}</span>
                                            @endif

                                            <span class="label label-warning">{{ $sell->packages->count() }} Paket</span>
                                        </td>
                                        <td>
                                            {{ $sell->created_at->format('d.m.Y H:i') }}
                                        </td>
                                        @if(auth()->user()->can('actions.sell.edit') || auth()->user()->can('actions.sell.delete') || auth()->user()->can('actions.sell.show'))
                                        <td>
                                            @permission('actions.sell.show')
                                            <a href="{{ route('actions.sell.show', $sell->id) }}" class="btn blue btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-eye"></i> <span class="hidden-480">Görüntüle</span>
                                            </a>
                                            @endpermission

                                            @if(!$sell->isLocked)
                                                @permission('actions.sell.edit')
                                                <a href="{{ route('actions.sell.edit', $sell->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                    <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                                </a>
                                                @endpermission

                                                @permission('actions.sell.delete')
                                                <a href="{{ route('actions.sell.destroy', $sell->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                    <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                                </a>
                                                @endpermission
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $sells->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>
                                Henüz bir satış girişi yapılmadı.
                                @permission('actions.sell.create')
                                Yeni bir <a href="{{ route('actions.sell.create') }}">satış girişi oluştur</a>abilirsiniz.
                                @endpermission
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop