@extends('layout.page')

@section('title', $sell->created_at->format('d.m.Y H:i')." Tarihli Satış Girişim")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.sell.index') }}">Satışlarım</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $sell->created_at->format('d.m.Y H:i')." Tarihli Satış Girişim" }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $sell->created_at->format('d.m.Y H:i')." Tarihli Satış Girişim" }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <h4>Satış Bilgileri</h4>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Satış Tarihi</td>
                            <td>{{ $sell->created_at->format("d.m.Y H:i") }}</td>
                        </tr>
                        <tr>
                            <td>Kampanya</td>
                            <td>
                                @if($sell->campaign)
                                    {{ $sell->campaign->name }}
                                @else
                                    Kampanya Yok
                                @endif
                            </td>
                        </tr>
                        @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>
                                    @foreach($sell->packages()->where('service_id', $service->id)->get() as $package)
                                        <span class="label label-info">{{ $package->name }}</span>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <h4>Daire Bilgileri</h4>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Lokasyon</td>
                            <td>
                                @if($sell->home->apartment->attachedTown)
                                    <span class="label label-info">{{ $sell->home->apartment->attachedTown->name }}</span>
                                @endif

                                @if($sell->home->apartment->attachedStreet)
                                    /
                                    <span class="label label-info">{{ $sell->home->apartment->attachedStreet->name }}</span>
                                @endif

                                @if($sell->home->apartment->district)
                                    /
                                    <span class="label label-info">{{ $sell->home->apartment->district->name }}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Apartman / Daire</td>
                            <td>{{ $sell->home->apartment->name }} / {{ $sell->home->home_no }}</td>
                        </tr>
                        <tr>
                            <td>Ad Soyad</td>
                            <td>{{ $sell->home->name." ".$sell->home->surname }}</td>
                        </tr>
                        <tr>
                            <td>T.C. Kimlik Numarası</td>
                            <td>{{ $sell->home->identity }}</td>
                        </tr>
                        <tr>
                            <td>Telefon Numarası</td>
                            <td>{{ $sell->home->phone }}</td>
                        </tr>
                        <tr>
                            <td>E-Posta Adresi</td>
                            <td>{{ $sell->home->email }}</td>
                        </tr>
                        <tr>
                            <td>Kullandığı Servis Sağlayıcı</td>
                            <td>{{ $sell->home->provider ? $sell->home->provider->name : "Kullanmıyor" }}</td>
                        </tr>
                        @if($sell->home->commitment)
                        <tr>
                            <td>Taahhüt Süresi</td>
                            <td>{{ Carbon\Carbon::createFromFormat("Y-m-d", $sell->home->commitment)->format("d.m.Y") }}</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop