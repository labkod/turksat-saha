@extends('layout.master')

@section('body.class', "login")

@section('body')
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{ url('/') }}">
            <img src="{{ asset('http://placehold.it/120x19?text=saha') }}" alt="" />
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        {!! Form::open(["route" => "auth.login", "method" => "post", "class" => "login-form"]) !!}
            <h3 class="form-title font-green">Giriş Yap</h3>

            @if(count($errors) > 0 )
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Lütfen bir kullanıcı adı ve parola belirtin. </span>
            </div>
            <div class="form-group">
                {!! Form::label("username", "Kullanıcı Adı", ["class" => "control-label visible-ie8 visible-ie9"]) !!}
                {!! Form::text("username", old("username"), ["class" => "form-control form-control-solid placeholder-no-fix", "autocomplete" => "off", "placeholder" => "Kullanıcı Adı"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label("password", "Parola", ["class" => "control-label visible-ie8 visible-ie9"]) !!}
                {!! Form::password("password", ["class" => "form-control form-control-solid placeholder-no-fix", "autocomplete" => "off", "placeholder" => "Parola"]) !!}
                <a href="javascript:;" id="forget-password" class="forget-password">Parolamı Unuttum?</a>
            </div>
            <div class="form-actions">
                {!! Form::button('Giriş Yap', ["class" => "btn green uppercase", "type" => "submit"]) !!}
                <label class="rememberme check pull-right">
                    {!! Form::checkbox("remember", true, old('remember')) !!}
                    Beni Hatırla
                </label>
            </div>
        {!! Form::close() !!}
        <!-- END LOGIN FORM -->
    </div>
    <div class="copyright"> {{ Carbon\Carbon::now()->year }} © <a href="http://labkod.com">Labkod</a> . {{ config('app.name') }}. </div>
@stop

@section('plugin.css')
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('page.css')
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('plugin.js')
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    <script src="{{ asset('assets/pages/scripts/login.min.js') }}" type="text/javascript"></script>
@stop