@extends('layout.page')

@section('title', "Hesap Ayarları")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Hesap Ayarları</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Hesap Ayarları</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => "auth.settings", "method" => "PUT"]) !!}

                    @include("util.error")

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('name', "Ad Soyad", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('name', auth()->user()->name, ["class" => "form-control", "placeholder" => "Ad Soyad"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('username', "Kullanıcı Adı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('username', auth()->user()->username, ["class" => "form-control", "placeholder" => "Kullanıcı Adı"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('email', "E-Posta Adresi", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-envelope-o fa-fw"></i>
                                    {!! Form::email('email', auth()->user()->email, ["class" => "form-control", "placeholder" => "E-Posta Adresi"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('password', "Parola (Değiştirmek İstemiyorsanız Boş Bırakın)", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-lock fa-fw"></i>
                                    {!! Form::password('password', ["class" => "form-control", "placeholder" => "Parola"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('password_confirmation', "Parola Onayı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-lock fa-fw"></i>
                                    {!! Form::password('password_confirmation', ["class" => "form-control", "placeholder" => "Parola Onayı"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Hesap Ayarlarımı Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-warning", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop