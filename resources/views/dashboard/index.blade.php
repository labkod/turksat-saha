@extends('layout.page')

@section('title', 'Pano')

@section('plugin.js')
    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
@stop

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Pano</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Pano
        <small>pano & genel istatistikler</small>
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD-->
    <div class="row">
        @permission('actions.apartment')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-building"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ auth()->user()->apartment_checks->count() }}">0</span>
                    </div>
                    <div class="desc"> Apartman Girişi Yaptınız </div>
                </div>
                <a class="more" href="{{ route('actions.apartment.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('list.apartment')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-building"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ \App\ApartmentCheck::count() }}">0</span>
                    </div>
                    <div class="desc"> Apartman Girişi </div>
                </div>
                <a class="more" href="{{ route('list.apartment.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('actions.home')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ auth()->user()->homes->count() }}">0</span></div>
                    <div class="desc"> Daire Girişi Yaptınız </div>
                </div>
                <a class="more" href="{{ route('actions.home.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('list.home')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ \App\Home::count() }}">0</span></div>
                    <div class="desc"> Daire Girişi </div>
                </div>
                <a class="more" href="{{ route('list.home.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('actions.sell')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ auth()->user()->sells->count() }}">0</span>
                    </div>
                    <div class="desc"> Satış Yaptınız </div>
                </div>
                <a class="more" href="{{ route('actions.sell.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('list.sell')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ \App\Sell::count() }}">0</span>
                    </div>
                    <div class="desc"> Satış </div>
                </div>
                <a class="more" href="{{ route('list.sell.index') }}"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
        @permission('manage.users')
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="fa fa-male"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ App\User::count()  }}">0</span>
                    </div>
                    <div class="desc"> Personel </div>
                </div>
                <a class="more" href="javascript:;"> Tümü
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        @endpermission
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD-->
@stop