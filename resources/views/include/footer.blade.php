<div class="page-footer">
    <div class="page-footer-inner"> {{ Carbon\Carbon::now()->year }} &copy; {{ config('app.name') }}.
        <a href="http://labkod.com" title="Labkod" target="_blank">Labkod</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>