<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            <li class="nav-item start active">
                <a href="javascript:;" class="nav-link">
                    <i class="icon-home"></i>
                    <span class="title">Pano</span>
                </a>
            </li>
            @permission('actions')
                <li class="heading">
                    <h3 class="uppercase">İşlemler</h3>
                </li>
                @permission('actions.create')
                <li class="nav-item">
                    <a href="{{ route('actions.create') }}" class="nav-link">
                        <i class="icon-rocket"></i>
                        <span class="title">Hızlı İşlem Girişi</span>
                    </a>
                </li>
                @endpermission
                @permission('actions.apartment')
                <li class="nav-item">
                    <a href="{{ route('actions.apartment.index') }}" class="nav-link">
                        <i class="icon-map"></i>
                        <span class="title">Apartmanlar</span>
                    </a>
                </li>
                @endpermission
                @permission('actions.home')
                <li class="nav-item">
                    <a href="{{ route('actions.home.index') }}" class="nav-link">
                        <i class="icon-user"></i>
                        <span class="title">Daireler</span>
                    </a>
                </li>
                @endpermission
                @permission('actions.sell')
                <li class="nav-item">
                    <a href="{{ route('actions.sell.index') }}" class="nav-link">
                        <i class="icon-credit-card"></i>
                        <span class="title">Satışlarım</span>
                    </a>
                </li>
                @endpermission
            @endpermission
            @permission('list')
                <li class="heading">
                    <h3 class="uppercase">Girişler</h3>
                </li>
                @permission('list.apartment')
                <li class="nav-item">
                    <a href="{{ route('list.apartment.index') }}" class="nav-link">
                        <i class="icon-map"></i>
                        <span class="title">Apartmanlar</span>
                    </a>
                </li>
                @endpermission
                @permission('list.home')
                <li class="nav-item">
                    <a href="{{ route('list.home.index') }}" class="nav-link">
                        <i class="icon-user"></i>
                        <span class="title">Daireler</span>
                    </a>
                </li>
                @endpermission
                @permission('list.sell')
                <li class="nav-item">
                    <a href="{{ route('list.sell.index') }}" class="nav-link">
                        <i class="icon-credit-card"></i>
                        <span class="title">Satışlar</span>
                    </a>
                </li>
                @endpermission
            @endpermission
            @permission('manage')
            <li class="heading">
                <h3 class="uppercase">Yönetim</h3>
            </li>
            @permission('manage.campaign')
            <li class="nav-item">
                <a href="{{ route('manage.campaign.index') }}" class="nav-link">
                    <i class="icon-basket-loaded"></i>
                    <span class="title">Kampanyalar</span>
                </a>
            </li>
            @endpermission
            @permission('manage.package')
            <li class="nav-item">
                <a href="{{ route('manage.package.index') }}" class="nav-link">
                    <i class="icon-folder"></i>
                    <span class="title">Paketler</span>
                </a>
            </li>
            @endpermission
            @permission('manage.users')
            <li class="nav-item">
                <a href="{{ route('manage.user.index') }}" class="nav-link">
                    <i class="icon-users"></i>
                    <span class="title">Personeller</span>
                </a>
            </li>
            @endpermission
            @permission('manage.locations')
            <li class="nav-item">
                <a href="{{ route('manage.location.index') }}" class="nav-link">
                    <i class="icon-map"></i>
                    <span class="title">Lokasyon Yönetimi</span>
                </a>
            </li>
            @endpermission
            @permission('manage.roles')
            <li class="nav-item">
                <a href="{{ route('manage.role.index') }}" class="nav-link">
                    <i class="icon-lock"></i>
                    <span class="title">Yetki Grupları</span>
                </a>
            </li>
            @endpermission
            @permission('manage.settings')
            <li class="nav-item">
                <a href="{{ route('manage.settings') }}" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">Sistem Ayarları</span>
                </a>
            </li>
            @endpermission
            @endpermission
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>