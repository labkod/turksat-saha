@extends('layout.page')

@section('title', $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Tarihli Giriş")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.index') }}">Apartmanlar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('actions.apartment.check.index', $apartment->id) }}">{{ $apartment->name }} Girişleri</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Tarihli Giriş" }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $apartment->name." - ".$check->created_at->format('d.m.Y H:i')." Tarihli Giriş" }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <h4>Bilgiler</h4>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tarih</td>
                            <td>{{ $check->created_at->format('d.m.Y H:i') }}</td>
                        </tr>
                        <tr>
                            <td>Personel</td>
                            <td>{{ $check->user->displayName }}</td>
                        </tr>
                        <tr>
                            <td>Notlar</td>
                            <td>{!! nl2br($check->notes) !!}</td>
                        </tr>
                        @if($check->options->count() > 0)
                        <tr>
                            <td>Seçenekler</td>
                            <td>
                                @foreach($check->options as $option)
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" checked disabled>
                                            {{ $option->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        @endif
                        </tbody>
                    </table>

                    @if($check->pictures->count() > 0)
                        <h4>Fotoğraflar</h4>
                        <div class="row">
                            @foreach($check->pictures as $picture)
                                <div class="col-md-4">
                                    <a title="{{ $picture->name }}" href="{{ asset($picture->file) }}" target="_blank">
                                        <img src="{{ asset($picture->file) }}" class="img-thumbnail img-responsive" style="height: 220px; max-width: 100%;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop