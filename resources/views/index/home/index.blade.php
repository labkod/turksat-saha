@extends('layout.page')

@section('title', "Daire Girişleri")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Daire Girişleri</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Daire Girişleri</span>
                    </div>
                    <div class="actions">
                        @if($homes->count() > 0)
                            @permission('actions.home.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('actions.home.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('actions.home.create')
                        <a class="btn green btn-outline" href="{{ route('actions.home.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Daire Girişi Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">

                    {!! Form::open(['route' => "list.home.index", "method" => "GET"]) !!}
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    {!! Form::select('town_id', [0 => "İlçe Seçiniz"] + \App\Town::orderBy('name', 'ASC')->pluck('name', 'id')->toArray(), $townId, ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $streets = [0 => "Önce İlçe Seçiniz"];

                                    if($townId){
                                        $town = \App\Town::find($townId);
                                        if($town){
                                            $streets = [0 => "Mahalle Seçiniz"] + $town->streets()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('street_id', $streets, $streetId, ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <?php
                                    $districts = [0 => "Önce İlçe Seçiniz"];

                                    if($streetId && isset($town)){
                                        $street = \App\Street::find($streetId);
                                        if($street){
                                            $districts = [0 => "Cadde/Sokak Seçiniz"] + $street->districts()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
                                        }
                                    }
                                    ?>
                                    {!! Form::select('district_id', $districts, $districtId, ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    {!! Form::select('apartment_id', $apartments, $apartmentId, ["class" => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-sm-6">
                                    {!! Form::button('Filtrele <i class="fa fa-search fa-fw"></i>', ["class" => "btn btn-block btn-primary", "type" => "submit"]) !!}
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{ route('list.home.index') }}" class="btn btn-warning btn-block">Temizle <i class="fa fa-trash fa-fw"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    @if($homes->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('actions.home.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th> İlçe / Mahalle / Sokak </th>
                                    <th> Apartman </th>
                                    <th> Personel </th>
                                    <th> Ad Soyad </th>
                                    <th> Giriş Tarihi </th>
                                    @if(auth()->user()->can('actions.home.edit') || auth()->user()->can('actions.home.delete') || auth()->user()->can('list.home.show'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($homes as $home)
                                    <tr>
                                        @permission('actions.home.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $home->id }}" class="table-select"></td>
                                        @endpermission
                                        <td>
                                            @if($home->apartment->attachedTown)
                                            <span class="label label-info">{{ $home->apartment->attachedTown->name }}</span>
                                            @endif

                                            @if($home->apartment->attachedStreet)
                                                /
                                            <span class="label label-info">{{ $home->apartment->attachedStreet->name }}</span>
                                            @endif

                                            @if($home->apartment->district)
                                                /
                                                <span class="label label-info">{{ $home->apartment->district->name }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $home->apartment->name }}
                                        </td>
                                        <td>
                                            {{ $home->user->displayName }}
                                        </td>
                                        <td>{{ $home->name." ".$home->surname }}</td>
                                        <td>
                                            {{ $home->created_at->format('d.m.Y H:i') }}
                                        </td>
                                        @if(auth()->user()->can('actions.home.edit') || auth()->user()->can('actions.home.delete') || auth()->user()->can('actions.home.show'))
                                        <td>
                                            @permission('list.home.show')
                                            <a href="{{ route('list.home.show', $home->id) }}" class="btn blue btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-eye"></i> <span class="hidden-480">Görüntüle</span>
                                            </a>
                                            @endpermission

                                            @permission('actions.home.edit')
                                            <a href="{{ route('actions.home.edit', $home->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                            </a>
                                            @endpermission

                                            @permission('actions.home.delete')
                                            <a href="{{ route('actions.home.destroy', $home->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                            </a>
                                            @endpermission
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $homes->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>
                                Henüz bir daire girişi yapılmadı.
                                @permission('actions.home.create')
                                Yeni bir <a href="{{ route('actions.home.create') }}">daire girişi oluştur</a>abilirsiniz.
                                @endpermission
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop