@extends('layout.page')

@section('title', $home->user->displayName." tarafından ".$home->created_at->format('d.m.Y H:i')." Tarihli Daire Girişi")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('list.home.index') }}">Daire Girişleri</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $home->user->displayName." tarafından ".$home->created_at->format('d.m.Y H:i')." Tarihli Daire Girişi" }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $home->user->displayName." tarafından ".$home->created_at->format('d.m.Y H:i')." Tarihli Daire Girişi" }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <h4>Daire Bilgileri</h4>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Lokasyon</td>
                            <td>
                                @if($home->apartment->attachedTown)
                                    <span class="label label-info">{{ $home->apartment->attachedTown->name }}</span>
                                @endif

                                @if($home->apartment->attachedStreet)
                                    /
                                    <span class="label label-info">{{ $home->apartment->attachedStreet->name }}</span>
                                @endif

                                @if($home->apartment->district)
                                    /
                                    <span class="label label-info">{{ $home->apartment->district->name }}</span>
                                @endif

                                @if($home->apartment)
                                    /
                                    <span class="label label-info">{{ $home->apartment->name }}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Daire Numarası</td>
                            <td>{{ $home->home_no }}</td>
                        </tr>
                        <tr>
                            <td>Ad Soyad</td>
                            <td>{{ $home->name." ".$home->surname }}</td>
                        </tr>
                        <tr>
                            <td>T.C. Kimlik Numarası</td>
                            <td>{{ $home->identity }}</td>
                        </tr>
                        <tr>
                            <td>Telefon Numarası</td>
                            <td>{{ $home->phone }}</td>
                        </tr>
                        <tr>
                            <td>E-Posta Adresi</td>
                            <td>{{ $home->email }}</td>
                        </tr>
                        <tr>
                            <td>Kullandığı Servis Sağlayıcı</td>
                            <td>{{ $home->provider ? $home->provider->name : "Kullanmıyor" }}</td>
                        </tr>
                        @if($home->commitment)
                        <tr>
                            <td>Taahhüt Süresi</td>
                            <td>{{ Carbon\Carbon::createFromFormat("Y-m-d", $home->commitment)->format("d.m.Y") }}</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                    <h4>Giriş Bilgileri</h4>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tarih</td>
                            <td>{{ $home->created_at->format('d.m.Y H:i') }}</td>
                        </tr>
                        <tr>
                            <td>Personel</td>
                            <td>{{ $home->user->displayName }}</td>
                        </tr>
                        <tr>
                            <td>Notlar</td>
                            <td>{!! nl2br($home->notes) !!}</td>
                        </tr>
                        </tbody>
                    </table>

                    @if($home->pictures->count() > 0)
                        <h4>Fotoğraflar</h4>
                        <div class="row">
                            @foreach($home->pictures as $picture)
                                <div class="col-md-4">
                                    <a title="{{ $picture->name }}" href="{{ asset($picture->file) }}" target="_blank">
                                        <img src="{{ asset($picture->file) }}" class="img-thumbnail img-responsive" style="height: 220px; max-width: 100%;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/location-wizard.js') }}"></script>
@stop