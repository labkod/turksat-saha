@extends('layout.page')

@section('title', "Yeni Kampanya Oluşturma")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.campaign.index') }}">Kampanyalar</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yeni Kampanya</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yeni Kampanya Ekle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.campaign.store"], "method" => "POST"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        {!! Form::label('name', "Kampanya Adı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-lock fa-fw"></i>
                            {!! Form::text('name', old('name'), ["class" => "form-control", "placeholder" => "Kampanya için bir isim giriniz"]) !!}
                        </div>
                    </div>
                        @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="border-bottom: 1px solid silver; padding-bottom: 5px;">{{ $service->name }}</h4>
                                </div>

                                @foreach($service->packages as $key => $package)
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>
                                                {!! Form::checkbox("packages[".$package->id."]", $package->id, old('packages.'.$package->id)) !!}
                                                {{ $package->name }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        @endforeach
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Kampanyayı Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
@stop