@extends('layout.page')

@section('title', "Kampanyalar")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Kampanyalar</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Kampanyalar</span>
                    </div>
                    <div class="actions">
                        @if($campaigns->count() > 0)
                            @permission('manage.campaign.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('manage.campaign.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('manage.campaign.create')
                        <a class="btn green btn-outline" href="{{ route('manage.campaign.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Kampanya Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($campaigns->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('manage.campaign.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th> Kampanya </th>
                                    @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                                        <th> {{ $service->name }} </th>
                                    @endforeach
                                    @if(auth()->user()->can('manage.campaign.edit') || auth()->user()->can('manage.campaign.delete'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($campaigns as $campaign)
                                    <tr>
                                        @permission('manage.campaign.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $campaign->id }}" class="table-select"></td>
                                        @endpermission
                                        <td>{{ $campaign->name }}</td>
                                        @foreach(\App\Service::orderBy('name', 'ASC')->get() as $service)
                                            <td>
                                                <span class="label @if($campaign->totalServicePackage($service) > 0) label-primary @else label-default @endif">{{ $campaign->totalServicePackage($service) }} <span class="hidden-480">Paket/Tarife</span></span>
                                            </td>
                                        @endforeach
                                        @if(auth()->user()->can('manage.campaign.edit') || auth()->user()->can('manage.campaign.delete'))
                                        <td>
                                            @permission('manage.campaign.edit')
                                            <a href="{{ route('manage.campaign.edit', $campaign->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                            </a>
                                            @endpermission

                                            @permission('manage.campaign.delete')
                                            <a href="{{ route('manage.campaign.destroy', $campaign->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                            </a>
                                            @endpermission
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $campaigns->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>
                                Henüz bir kampanya oluşturulmadı.
                                @permission('manage.campaign.create')
                                Yeni bir <a href="{{ route('manage.campaign.create') }}">kampanya oluştur</a>abilirsiniz.
                                @endpermission
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop