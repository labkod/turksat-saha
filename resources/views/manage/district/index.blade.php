@extends('layout.page')

@section('title', $street->name." Sokakları")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Lokasyon Yönetimi</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.location.index') }}">İlçeler</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.location.street.index', $town->id) }}">{{ $town->name }} Sokakları</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $street->name }} Sokakları</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Lokasyon Yönetimi
        <small>ilçeler, mahalleler, sokaklar</small>
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $street->name }} Sokakları</span>
                    </div>
                    <div class="actions">
                        <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('manage.location.street.district.destroyMulti', [$town->id, $street->id]) }}">
                            <i class="icon-trash"></i>
                            <span class="hidden-480">Seçilileri Sil</span>
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.location.street.district.store", $town->id, $street->id]]) !!}

                    @include("util.error")

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-icon">
                                <i class="fa fa-map-marker fa-fw"></i>
                                {!! Form::text('name', old('name'), ["class" => "form-control", "placeholder" => "Sokak İsmi"]) !!}
                            </div>
                            <span class="input-group-btn">
                                {!! Form::button('Yeni Sokak Ekle <i class="fa fa-check fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                            </span>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <div class="table-scrollable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th width="7%">
                                    <label class="table-select-label">
                                        <input type="checkbox" name="selectAll" class="table-select-all">
                                        <span class="hidden-480">Tümünü Seç</span>
                                    </label>
                                </th>
                                <th> İsim </th>
                                <th width="45%">İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($districts as $district)
                            <tr>
                                <td><input type="checkbox" name="select[]" value="{{ $district->id }}" class="table-select"></td>
                                <td>{{ $district->name }}</td>
                                <td>
                                    <a href="{{ route('manage.location.street.district.edit', [$town->id, $street->id, $district->id]) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                        <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                    </a>

                                    <a href="{{ route('manage.location.street.district.destroy', [$town->id, $street->id, $district->id]) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                        <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center">
                        {!! $districts->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop