@extends('layout.page')

@section('title', "Paket/Tarife Düzenleme")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.package.index') }}">Paketler</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Paket/Tarife Düzenleme</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Paket/Tarife Düzenleme</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.package.update", $package->id], "method" => "PUT"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        {!! Form::label('name', "Paket/Tarife Adı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-lock fa-fw"></i>
                            {!! Form::text('name', old('name') ? old('name') : $package->name, ["class" => "form-control", "placeholder" => "Paket/tarife için bir isim giriniz"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('service_id', "Servis", ["class" => "control-label"]) !!}
                        {!! Form::select('service_id', \App\Service::orderBy('name', 'ASC')->pluck('name', 'id'), old('service_id') ? old('service_id') : $package->service->id, ["class" => "form-control"]) !!}
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Paketi Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-warning", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
@stop