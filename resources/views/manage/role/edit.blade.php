@extends('layout.page')

@section('title', $role->display_name." Yetki Grubunu Düzenle")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.role.index') }}">Yetki Grupları</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $role->display_name }} Yetki Grubunu Düzenle</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yetki Grubunu Düzenle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.role.update", $role->id], "method" => "PUT"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        {!! Form::label('name', "Yetki Grubu Adı", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-lock fa-fw"></i>
                            {!! Form::text('name', old('name') ? old('name') : $role->display_name, ["class" => "form-control", "placeholder" => "Yetki grubu için bir isim giriniz"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('users[]', "Kullanıcılar", ["class" => "control-label"]) !!}
                        {!! Form::select('users[]', \App\User::orderBy('username', 'ASC')->pluck('username', 'id'), old('users') ? old('users') : $role->users->pluck('id')->toArray(), ["class" => "form-control", "multiple"]) !!}
                    </div>

                    <hr>

                    <button data-hide-content="#permissionsList" data-show-text="Yetkileri Gizle" data-hide-text="Yetkileri Göster" class="btn btn-primary">Yetkileri Göster</button>
                    <div id="permissionsList">
                        <br>
                        <div class="row">
                        @foreach(\App\Permission::orderBy('name', 'ASC')->get() as $key => $permission)
                            @if(strpos($permission->name, ".") == 0)
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 style="border-bottom: 1px solid silver; padding-bottom: 5px;">
                                            <label>
                                                {!! Form::checkbox("permissions[".$key."]", $permission->id, old('permissions.'.$key) ? old('permissions.'.$key) : $role->perms()->where('id', $permission->id)->count(), ["class" => "group-check"]) !!}
                                                {{ $permission->display_name }}
                                                <abbr title="{{ $permission->description }}"><i class="fa fa-question-circle"></i></abbr>
                                            </label>
                                        </h4>
                                    </div>
                            @else
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>
                                            {!! Form::checkbox("permissions[".$key."]", $permission->id, old('permissions.'.$key) ? old('permissions.'.$key) : $role->perms()->where('id', $permission->id)->count(), ["class" => "parent-check"]) !!}
                                            {{ $permission->display_name }}
                                            <abbr title="{{ $permission->description }}"><i class="fa fa-question-circle"></i></abbr>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        </div>
                    </div>

                    <hr>

                    <div class="form-group text-center">
                        {!! Form::button('Yetki Grubu Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-warning", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
    <script>
        $(".parent-check").change(function(){
            var parent = $(this).closest('.row').find('.group-check');
            if(this.checked){
                parent.prop('checked', true);
                parent.parent().addClass('checked');
            } else {
                var least = false;
                var row = $(this).closest('.row');
                $('.col-sm-4', row).each(function(i, item){
                    if($(item).find('.parent-check').prop('checked')){
                        least = true;
                    }
                });

                if(!least){
                    parent.prop('checked', false);
                    parent.parent().removeClass('checked');
                }
            }
        });
    </script>
@stop