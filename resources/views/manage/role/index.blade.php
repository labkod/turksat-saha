@extends('layout.page')

@section('title', "Yetki Grupları")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yetki Grupları</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yetki Grupları</span>
                    </div>
                    <div class="actions">
                        @if($roles->count() > 0)
                            @permission('manage.roles.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('manage.role.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('manage.roles.create')
                        <a class="btn green btn-outline" href="{{ route('manage.role.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Yetki Grubu Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($roles->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('manage.roles.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th width="15%"> Adı </th>
                                    <th> Kullanıcılar </th>
                                    <th> Temel Yetkiler </th>
                                    @if(auth()->user()->can('manage.roles.edit') || auth()->user()->can('manage.roles.delete'))
                                        <th width="45%">İşlemler</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        @permission('manage.roles.delete')
                                        <td><input type="checkbox" name="select[]" value="{{ $role->id }}" class="table-select"></td>
                                        @endpermission
                                        <td>{{ $role->display_name }}</td>
                                        <td>
                                            @if($role->users->count() > 0)
                                                @foreach($role->users as $user)
                                                    @if(auth()->user()->can('manage.users.edit'))
                                                        <a href="{{ route('manage.user.edit', $user->id) }}" class="label label-primary">
                                                            {{ $user->display_name }}
                                                        </a>
                                                    @else
                                                        <span class="label label-primary">
                                                            {{ $user->display_name }}
                                                        </span>
                                                    @endif
                                                @endforeach
                                            @else
                                                <span class="label label-danger">Kullanıcı Yok</span>
                                            @endif
                                        </td>
                                        <td>

                                            @if($role->perms()->where('name', 'actions')->count() > 0)
                                                <abbr title="Giriş Oluşturma Yetkisi" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil fa-fw"></i>
                                                </abbr>
                                            @else
                                                <abbr title="Giriş Oluşturma Yetkisi" class="btn btn-default btn-sm" disabled>
                                                    <i class="fa fa-times fa-fw"></i>
                                                </abbr>
                                            @endif

                                            @if($role->perms()->where('name', 'list')->count() > 0)
                                                <abbr title="Girişleri Görüntüleme Yetkisi" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-list fa-fw"></i>
                                                </abbr>
                                            @else
                                                <abbr title="Girişleri Görüntüleme Yetkisi" class="btn btn-default btn-sm" disabled>
                                                    <i class="fa fa-times fa-fw"></i>
                                                </abbr>
                                            @endif

                                            @if($role->perms()->where('name', 'manage')->count() > 0)
                                                <abbr title="Yönetim Yetkisi" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-wrench fa-fw"></i>
                                                </abbr>
                                            @else
                                                <abbr title="Yönetim Yetkisi" class="btn btn-default btn-sm" disabled>
                                                    <i class="fa fa-times fa-fw"></i>
                                                </abbr>
                                            @endif
                                        </td>
                                        @if(auth()->user()->can('manage.roles.edit') || auth()->user()->can('manage.roles.delete'))
                                        <td>
                                            @permission('manage.roles.edit')
                                            <a href="{{ route('manage.role.edit', $role->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                            </a>
                                            @endpermission

                                            @permission('manage.roles.delete')
                                            <a href="{{ route('manage.role.destroy', $role->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                            </a>
                                            @endpermission
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $roles->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>Düzenlenecek bir yetki grubu oluşturulmadı. Yeni bir <a href="{{ route('manage.role.create') }}">yetki grubu oluştur</a>abilirsiniz.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop