@extends('layout.page')

@section('title', "Sistem Ayarları")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Sistem Ayarları</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Sistem Ayarları</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.settings"], "method" => "PUT"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        {!! Form::label('app_name', "Program İsmi", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>
                            {!! Form::text('app_name', old('app_name') ? old('app_name') : setting('app_name', config('app.name')), ["class" => "form-control", "placeholder" => "Arayüz kısmında göstermek istediğiniz program ismidir."]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('action_lock', "Giriş Düzenleme/Silme Kitleme Süresi (Dakika)", ["class" => "control-label"]) !!}
                        <div class="input-icon">
                            <i class="fa fa-lock fa-fw"></i>
                            {!! Form::number('action_lock', old('action_lock') ? old('action_lock') : setting('action_lock', 60), ["class" => "form-control", "placeholder" => "Kayıt giren personellerin güncelleme ve silme için yetkilerinin olacağı süredir."]) !!}
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Ayarları Kaydet <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('page.js')
    @parent
@stop