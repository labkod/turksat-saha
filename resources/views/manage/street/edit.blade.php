@extends('layout.page')

@section('title', "'".$street->name."' Mahalle Düzenle")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Lokasyon Yönetimi</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.location.index') }}">İlçeler</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.location.street.index', $town->id) }}">{{ $town->name }} Mahalleleri</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ $street->name }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Lokasyon Yönetimi
        <small>ilçeler, mahalleler, sokaklar</small>
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $street->name }} MAHALLESİNİ DÜZENLE</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.location.street.update", $town->id, $street->id], "method" => "PUT"]) !!}

                    @include("util.error")

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-icon">
                                <i class="fa fa-map-marker fa-fw"></i>
                                {!! Form::text('name', old('name') ? old('name') : $street->name, ["class" => "form-control", "placeholder" => "Mahalle İsmi"]) !!}
                            </div>
                            <span class="input-group-btn">
                                {!! Form::button('Mahalleyi Güncelle <i class="fa fa-refresh fa-fw"></i>', ["class" => "btn btn-warning", "type" => "submit"]) !!}
                            </span>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop