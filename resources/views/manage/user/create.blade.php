@extends('layout.page')

@section('title', "Yeni Personel Oluşturma")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('manage.user.index') }}">Personeller</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Yeni Personel</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
<br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Yeni Personel Ekle</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(["route" => ["manage.user.store"], "method" => "POST"]) !!}

                    @include("util.error")

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('name', "Ad Soyad", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('name', old('name'), ["class" => "form-control", "placeholder" => "Ad Soyad"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('username', "Kullanıcı Adı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-user fa-fw"></i>
                                    {!! Form::text('username', old('username'), ["class" => "form-control", "placeholder" => "Kullanıcı Adı"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('email', "E-Posta Adresi", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-envelope-o fa-fw"></i>
                                    {!! Form::email('email', old('email'), ["class" => "form-control", "placeholder" => "E-Posta Adresi"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('password', "Parola", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-lock fa-fw"></i>
                                    {!! Form::password('password', ["class" => "form-control", "placeholder" => "Parola"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('password_confirmation', "Parola Onayı", ["class" => "control-label"]) !!}
                                <div class="input-icon">
                                    <i class="fa fa-lock fa-fw"></i>
                                    {!! Form::password('password_confirmation', ["class" => "form-control", "placeholder" => "Parola Onayı"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('groups[]', "Personel Grupları", ["class" => "control-label"]) !!}
                                {!! Form::select('groups[]', \App\UserGroup::orderBy('name', 'ASC')->pluck('name', 'id'), old('groups'), ["class" => "form-control", "multiple"]) !!}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('roles[]', "Yetki Grupları", ["class" => "control-label"]) !!}
                                {!! Form::select('roles[]', \App\Role::orderBy('name', 'ASC')->pluck('display_name', 'id'), old('roles'), ["class" => "form-control", "multiple"]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        {!! Form::button('Personel Kaydet <i class="fa fa-plus fa-fw"></i>', ["class" => "btn btn-success", "type" => "submit"]) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop