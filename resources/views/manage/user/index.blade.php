@extends('layout.page')

@section('title', "Personeller")

@section('page')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/') }}">{{ config('app.name') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Personeller</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <br>
    <!-- BEGIN TABLE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Personeller</span>
                    </div>
                    <div class="actions">
                        @if($users->count() > 0)
                            @permission('manage.users.delete')
                            <button type="button" class="btn red btn-outline btn-delete-all" data-url="{{ route('manage.user.destroyMulti') }}">
                                <i class="icon-trash"></i>
                                <span class="hidden-480">Seçilileri Sil</span>
                            </button>
                            @endpermission
                        @endif
                        @permission('manage.users.create')
                        <a class="btn green btn-outline" href="{{ route('manage.user.create') }}">
                            <i class="icon-plus"></i>
                            <span class="hidden-480">Yeni Personel Ekle</span>
                        </a>
                        @endpermission
                    </div>
                </div>
                <div class="portlet-body">
                    @if($users->count() > 0)
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    @permission('manage.users.delete')
                                    <th width="7%">
                                        <label class="table-select-label">
                                            <input type="checkbox" name="selectAll" class="table-select-all">
                                            <span class="hidden-480">Tümünü Seç</span>
                                        </label>
                                    </th>
                                    @endpermission
                                    <th> Ad Soyad </th>
                                    <th> Kullanıcı Adı </th>
                                    <th> Gruplar </th>
                                    <th> Roller </th>
                                    <th width="45%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td><input type="checkbox" name="select[]" value="{{ $user->id }}" class="table-select"></td>
                                        <td>
                                            @if($user->name)
                                                {{ $user->name }}
                                            @else
                                                <span class="label label-warning">Ad Soyad Belirtilmedi</span>
                                            @endif
                                        </td>
                                        <td>{{ $user->username }}</td>
                                        <td>
                                            @if($user->groups->count() > 0)
                                                @foreach($user->groups as $group)
                                                    <span class="label label-primary">
                                            {{ $group->name }}
                                                        @if($group->pivot->is_admin)
                                                            (Yetkili)
                                                        @endif
                                        </span>
                                                @endforeach
                                            @else
                                                <span class="label label-danger">Grup Yok</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->roles->count() > 0)
                                                @foreach($user->roles as $role)
                                                    <span class="label label-primary">
                                            {{ $role->display_name }}
                                        </span>
                                                @endforeach
                                            @else
                                                <span class="label label-danger">Rol Yok</span>
                                            @endif
                                        </td>
                                        <td>
                                            @permission('manage.users.edit')
                                            <a href="{{ route('manage.user.edit', $user->id) }}" class="btn yellow btn-sm btn-outline sbold uppercase">
                                                <i class="fa fa-pencil"></i> <span class="hidden-480">Düzenle</span>
                                            </a>
                                            @endpermission

                                            @permission('manage.users.delete')
                                            <a href="{{ route('manage.user.destroy', $user->id) }}" class="btn red btn-sm btn-outline sbold uppercase delete-confirm">
                                                <i class="fa fa-trash"></i> <span class="hidden-480">Sil</span>
                                            </a>
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $users->render() !!}
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>Düzenlenecek bir personel kayıt edilmedi. Yeni bir <a href="{{ route('manage.user.create') }}">personel oluştur</a>abilirsiniz.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->
@stop

@section('plugin.js')
    @parent
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@stop

@section('page.js')
    @parent
    <script src="{{ asset('assets/pages/scripts/table-wizard.js') }}"></script>
@stop