<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::model('town', 'App\Town');
Route::model('apartment_check', 'App\ApartmentCheck');

/**
 * Main Route Group
 */
Route::group([], function(){

    /**
     * Public Routes
     */
    // ...

    /**
     * Only Authenticated Users Routes
     */
    Route::group(["middleware" => "auth"], function(){

        // Dashboard Route
        Route::get("/", ["as" => "dashboard.index", "uses" => "DashboardController@index"]);

        /**
         * 'Actions' permission routes
         */
        Route::group(['middleware' => ['permission:actions*'], "prefix" => "action", "as" => "actions.", "namespace" => "Action"], function(){

            // 'Actions Create' route for multiple create actions.
            Route::get('create', ["as" => "create", "middleware" => ['permission:actions.create'], "uses" => "ActionsController@create"]);
            Route::post('create', ["as" => "store", "middleware" => ['permission:actions.create'], "uses" => "ActionsController@store"]);

            /**
             * 'Apartment Actions' permission routes
             */
            Route::group(['middleware' => ['permission:actions.apartment*'], 'prefix' => "apartment", "as" => "apartment."], function(){

                Route::get('/',                  ["as" => "index",   "uses" => "ApartmentController@index"]);
                Route::get("create",             ["as" => "create",  "uses" => "ApartmentController@create",  "middleware" => "permission:actions.apartment.create"]);
                Route::post("/",                 ["as" => "store",   "uses" => "ApartmentController@store",   "middleware" => "permission:actions.apartment.create"]);
                Route::get("{apartment}",        ["as" => "show",    "uses" => "ApartmentController@show"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "ApartmentController@destroyMultiple"]);
                Route::get("{apartment}/edit",   ["as" => "edit",    "uses" => "ApartmentController@edit",    "middleware" => "permission:actions.apartment.edit"]);
                Route::put("{apartment}",        ["as" => "update",  "uses" => "ApartmentController@update",  "middleware" => "permission:actions.apartment.edit"]);
                Route::get("{apartment}/delete", ["as" => "destroy", "uses" => "ApartmentController@destroy", "middleware" => "permission:actions.apartment.delete"]);

                Route::group(["middleware" => ['permission:actions.apartment.check*'], "prefix" => "{apartment}/check", "as" => "check."], function(){
                    Route::get('/',                  ["as" => "index",   "uses" => "ApartmentCheckController@index"]);
                    Route::get("create",             ["as" => "create",  "uses" => "ApartmentCheckController@create",  "middleware" => "permission:actions.apartment.create"]);
                    Route::post("/",                 ["as" => "store",   "uses" => "ApartmentCheckController@store",   "middleware" => "permission:actions.apartment.create"]);
                    Route::get("{apartment_check}",        ["as" => "show",    "uses" => "ApartmentCheckController@show"]);
                    Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "ApartmentCheckController@destroyMultiple"]);
                    Route::get("{apartment_check}/edit",   ["as" => "edit",    "uses" => "ApartmentCheckController@edit",    "middleware" => "permission:actions.apartment.edit"]);
                    Route::put("{apartment_check}",        ["as" => "update",  "uses" => "ApartmentCheckController@update",  "middleware" => "permission:actions.apartment.edit"]);
                    Route::get("{apartment_check}/delete", ["as" => "destroy", "uses" => "ApartmentCheckController@destroy", "middleware" => "permission:actions.apartment.delete"]);
                });

            });

            /**
             * 'Home Actions' permission routes
             */
            Route::group(['middleware' => ['permission:actions.home*'], 'prefix' => "home", "as" => "home."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "HomeController@index"]);
                Route::get("create",        ["as" => "create",  "uses" => "HomeController@create",  "middleware" => "permission:actions.home.create"]);
                Route::post("/",            ["as" => "store",   "uses" => "HomeController@store",   "middleware" => "permission:actions.home.create"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "HomeController@destroyMultiple", "middleware" => "permission:actions.home.delete"]);
                Route::get("{home}",        ["as" => "show",    "uses" => "HomeController@show"]);
                Route::get("{home}/edit",   ["as" => "edit",    "uses" => "HomeController@edit",    "middleware" => "permission:actions.home.edit"]);
                Route::put("{home}",        ["as" => "update",  "uses" => "HomeController@update",  "middleware" => "permission:actions.home.edit"]);
                Route::get("{home}/delete", ["as" => "destroy", "uses" => "HomeController@destroy", "middleware" => "permission:actions.home.delete"]);

            });

            /**
             * 'Sell Actions' permission routes
             */
            Route::group(['middleware' => ['permission:actions.sell*'], 'prefix' => "sell", "as" => "sell."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "SellController@index"]);
                Route::get("create",        ["as" => "create",  "uses" => "SellController@create",  "middleware" => "permission:actions.sell.create"]);
                Route::post("/",            ["as" => "store",   "uses" => "SellController@store",   "middleware" => "permission:actions.sell.create"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "SellController@destroyMultiple", "middleware" => "permission:actions.sell.delete"]);
                Route::get("{sell}",        ["as" => "show",    "uses" => "SellController@show"]);
                Route::get("{sell}/edit",   ["as" => "edit",    "uses" => "SellController@edit",    "middleware" => "permission:actions.sell.edit"]);
                Route::put("{sell}",        ["as" => "update",  "uses" => "SellController@update",  "middleware" => "permission:actions.sell.edit"]);
                Route::get("{sell}/delete", ["as" => "destroy", "uses" => "SellController@destroy", "middleware" => "permission:actions.sell.delete"]);

            });

            Route::group(["as" => "util.", "prefix" => "util"], function(){
                Route::get('apartments', ["as" => "apartments", "uses" => "UtilController@apartments"]);
                Route::get('options', ['as' => 'options', "uses" => "UtilController@options"]);
            });
        });

        /**
         * 'List' permission routes
         */
        Route::group(['middleware' => ['permission:list*'], "prefix" => "list", "as" => "list.", "namespace" => "Index"], function(){

            /**
             * 'Apartment List' permission routes
             */
            Route::group(['middleware' => ['permission:list.apartment*'], 'prefix' => "apartment", "as" => "apartment."], function(){

                Route::get('/',                  ["as" => "index",   "uses" => "ApartmentController@index"]);
                Route::get("{apartment}",        ["as" => "show",    "uses" => "ApartmentController@show"]);
                Route::get("{apartment}/edit",   ["as" => "edit",    "uses" => "ApartmentController@edit",    "middleware" => "permission:list.apartment.edit"]);
                Route::put("{apartment}",        ["as" => "update",  "uses" => "ApartmentController@update",  "middleware" => "permission:list.apartment.edit"]);
                Route::get("{apartment}/delete", ["as" => "destroy", "uses" => "ApartmentController@destroy", "middleware" => "permission:list.apartment.delete"]);

                Route::group(["middleware" => ['permission:list.apartment.check*'], "prefix" => "{apartment}/check", "as" => "check."], function(){
                    Route::get('/',                 ["as" => "index",   "uses" => "ApartmentCheckController@index"]);
                    Route::get("{apartment_check}", ["as" => "show",    "uses" => "ApartmentCheckController@show"]);
                });

            });

            /**
             * 'Home List' permission routes
             */
            Route::group(['middleware' => ['permission:list.home*'], 'prefix' => "home", "as" => "home."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "HomeController@index"]);
                Route::get("{home}",        ["as" => "show",    "uses" => "HomeController@show"]);
                Route::get("{home}/edit",   ["as" => "edit",    "uses" => "HomeController@edit",    "middleware" => "permission:list.home.edit"]);
                Route::put("{home}",        ["as" => "update",  "uses" => "HomeController@update",  "middleware" => "permission:list.home.edit"]);
                Route::get("{home}/delete", ["as" => "destroy", "uses" => "HomeController@destroy", "middleware" => "permission:list.home.delete"]);

            });

            /**
             * 'Sell List' permission routes
             */
            Route::group(['middleware' => ['permission:list.sell*'], 'prefix' => "sell", "as" => "sell."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "SellController@index"]);
                Route::get("{sell}",        ["as" => "show",    "uses" => "SellController@show"]);
                Route::get("{sell}/edit",   ["as" => "edit",    "uses" => "SellController@edit",    "middleware" => "permission:list.sell.edit"]);
                Route::put("{sell}",        ["as" => "update",  "uses" => "SellController@update",  "middleware" => "permission:list.sell.edit"]);
                Route::get("{sell}/delete", ["as" => "destroy", "uses" => "SellController@destroy", "middleware" => "permission:list.sell.delete"]);

            });

        });

        /**
         * 'Manage' permission routes
         */
        Route::group(['middleware' => ['permission:manage*'], "prefix" => "manage", "as" => "manage.", "namespace" => "Manage"], function(){

            /**
             * 'Manage Campaigns' permission routes
             */
            Route::group(['middleware' => ['permission:manage.campaign*'], 'prefix' => "campaign", "as" => "campaign."], function(){

                Route::get('/',                 ["as" => "index",   "uses" => "CampaignController@index"]);
                Route::get('create',            ["as" => "create",  "uses" => "CampaignController@create",  "middleware" => "permission:manage.campaign.create"]);
                Route::post('/',                ["as" => "store",   "uses" => "CampaignController@store",   "middleware" => "permission:manage.campaign.create"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "CampaignController@destroyMultiple"]);
                Route::get("{campaign}",        ["as" => "show",    "uses" => "CampaignController@show"]);
                Route::get("{campaign}/edit",   ["as" => "edit",    "uses" => "CampaignController@edit",    "middleware" => "permission:manage.campaign.edit"]);
                Route::put("{campaign}",        ["as" => "update",  "uses" => "CampaignController@update",  "middleware" => "permission:manage.campaign.edit"]);
                Route::get("{campaign}/delete", ["as" => "destroy", "uses" => "CampaignController@destroy", "middleware" => "permission:manage.campaign.delete"]);

            });

            /**
             * 'Manage Packages' permission routes
             */
            Route::group(['middleware' => ['permission:manage.package*'], 'prefix' => "package", "as" => "package."], function(){

                Route::get('/',                ["as" => "index",   "uses" => "PackageController@index"]);
                Route::get('create',           ["as" => "create",  "uses" => "PackageController@create",  "middleware" => "permission:manage.package.create"]);
                Route::post('/',               ["as" => "store",   "uses" => "PackageController@store",   "middleware" => "permission:manage.package.create"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "PackageController@destroyMultiple"]);
                Route::get("{package}",        ["as" => "show",    "uses" => "PackageController@show"]);
                Route::get("{package}/edit",   ["as" => "edit",    "uses" => "PackageController@edit",    "middleware" => "permission:manage.package.edit"]);
                Route::put("{package}",        ["as" => "update",  "uses" => "PackageController@update",  "middleware" => "permission:manage.package.edit"]);
                Route::get("{package}/delete", ["as" => "destroy", "uses" => "PackageController@destroy", "middleware" => "permission:manage.package.delete"]);

            });

            /**
             * 'Manage Users' permission routes
             */
            Route::group(['middleware' => ['permission:manage.users*'], 'prefix' => "user", "as" => "user."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "UserController@index"]);
                Route::get('create',        ["as" => "create",  "uses" => "UserController@create",  "middleware" => "permission:manage.users.create"]);
                Route::post('/',            ["as" => "store",   "uses" => "UserController@store",   "middleware" => "permission:manage.users.create"]);
                Route::delete("multi-delete", ["as" => "destroyMulti", "uses" => "UserController@destroyMultiple"]);
                Route::get("{user}",        ["as" => "show",    "uses" => "UserController@show"]);
                Route::get("{user}/edit",   ["as" => "edit",    "uses" => "UserController@edit",    "middleware" => "permission:manage.users.edit"]);
                Route::put("{user}",        ["as" => "update",  "uses" => "UserController@update",  "middleware" => "permission:manage.users.edit"]);
                Route::get("{user}/delete", ["as" => "destroy", "uses" => "UserController@destroy", "middleware" => "permission:manage.users.delete"]);

            });

            /**
             * 'Manage Locations' permission routes
             */
            // Towns
            Route::group(['middleware' => ['permission:manage.locations*'], 'prefix' => "location", "as" => "location."], function(){

                Route::get('/',               ["as" => "index",   "uses" => "TownController@index"]);
                Route::delete("multi-delete", ["as" => "destroyMulti", "uses" => "TownController@destroyMultiple"]);
                //Route::get('create',          ["as" => "create",  "uses" => "TownController@create"]);
                Route::post('/',              ["as" => "store",   "uses" => "TownController@store"]);
                Route::get("{town}/edit",     ["as" => "edit",    "uses" => "TownController@edit"]);
                Route::put("{town}",          ["as" => "update",  "uses" => "TownController@update"]);
                Route::get("{town}/delete",   ["as" => "destroy", "uses" => "TownController@destroy"]);

                // Streets
                Route::group(["prefix" => "{town}/street", "as" => "street."], function(){
                    Route::get('/',               ["as" => "index",   "uses" => "StreetController@index"]);
                    Route::delete("multi-delete", ["as" => "destroyMulti", "uses" => "StreetController@destroyMultiple"]);
                    Route::get('create',          ["as" => "create",  "uses" => "StreetController@create"]);
                    Route::post('/',              ["as" => "store",   "uses" => "StreetController@store"]);
                    Route::get("{street}/edit",   ["as" => "edit",    "uses" => "StreetController@edit"]);
                    Route::put("{street}",        ["as" => "update",  "uses" => "StreetController@update"]);
                    Route::get("{street}/delete", ["as" => "destroy", "uses" => "StreetController@destroy"]);

                    // Districts
                    Route::group(["prefix" => "{street}/district", "as" => "district."], function(){
                        Route::get('/',                 ["as" => "index",   "uses" => "DistrictController@index"]);
                        Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "DistrictController@destroyMultiple"]);
                        Route::get('create',            ["as" => "create",  "uses" => "DistrictController@create"]);
                        Route::post('/',                ["as" => "store",   "uses" => "DistrictController@store"]);
                        Route::get("{district}/edit",   ["as" => "edit",    "uses" => "DistrictController@edit"]);
                        Route::put("{district}",        ["as" => "update",  "uses" => "DistrictController@update"]);
                        Route::get("{district}/delete", ["as" => "destroy", "uses" => "DistrictController@destroy"]);
                    });
                });

            });

            /**
             * 'Manage Roles' permission routes
             */
            Route::group(['middleware' => ['permission:manage.roles*'], 'prefix' => "role", "as" => "role."], function(){

                Route::get('/',             ["as" => "index",   "uses" => "RoleController@index"]);
                Route::delete("multi-delete",   ["as" => "destroyMulti", "uses" => "RoleController@destroyMultiple"]);
                Route::get('create',        ["as" => "create",  "uses" => "RoleController@create"]);
                Route::post('/',            ["as" => "store",   "uses" => "RoleController@store"]);
                Route::get("{role}/edit",   ["as" => "edit",    "uses" => "RoleController@edit"]);
                Route::put("{role}",        ["as" => "update",  "uses" => "RoleController@update"]);
                Route::get("{role}/delete", ["as" => "destroy", "uses" => "RoleController@destroy"]);

            });

            // 'Manage Settings' permission route
            Route::get('settings', ["as" => "settings", "uses" => "SettingsController@edit",   "middleware" => "permission:manage.settings"]);
            Route::put('settings', ["as" => "settings", "uses" => "SettingsController@update", "middleware" => "permission:manage.settings"]);
        });

        Route::group(["as" => "util.", "prefix" => "util"], function(){
            Route::get("streets", ["as" => "streets", "uses" => "UtilController@streets"]);
            Route::get("districts", ["as" => "districts", "uses" => "UtilController@districts"]);
            Route::get("apartments", ["as" => "apartments", "uses" => "UtilController@apartments"]);
            Route::get("homes", ["as" => "homes", "uses" => "UtilController@homes"]);
            Route::get("campaign", ["as" => "campaign", "uses" => "UtilController@campaign"]);
        });

        // Authenticate Route for auth
        Route::get("settings", ["as" => "auth.settings", "uses" => "AuthController@settings"]);
        Route::put("settings", ["as" => "auth.settings", "uses" => "AuthController@updateSettings"]);
        Route::get("logout", ["as" => "auth.logout", "uses" => "AuthController@logout"]);

    });

    /**
     * Only NOT Authenticated User Routes
     */
    Route::group(["middleware" => "guest"], function(){

        // Authenticate Route for guest
        Route::get("login", ["as" => "login", "uses" => "AuthController@login"]);
        Route::post("login", ["as" => "auth.login", "uses" => "AuthController@doLogin"]);

    });

});
